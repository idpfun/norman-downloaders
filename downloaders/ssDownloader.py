import os,md5,requests,sys,pprint,json,time

class ssDownloader():
	
	def __init__(self):
		self.options = {}
		self.options["wait_time"] = 0.01
		self.options["data_path"] = ""
		self.options["remake_age"] = 1800
		
	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##
	def check_directory(self):
		if not os.path.exists(os.path.join(self.options["data_path"], "ss")):
			os.mkdir(os.path.join(self.options["data_path"], "ss"))
		
	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##
	
	
	def movePPII(self,ppii_directory):
		self.check_directory()
		
		self.options["ss_data_path"] = os.path.join(self.options["data_path"],"ss")
		if not os.path.exists(os.path.join(self.options["ss_data_path"])):
			os.mkdir(os.path.join(os.path.join(self.options["ss_data_path"])))
		
		self.options["ppii_data_path"] = os.path.join(self.options["ss_data_path"],"ppii")
		if not os.path.exists(os.path.join(self.options["ppii_data_path"])):
			os.mkdir(os.path.join(os.path.join(self.options["ppii_data_path"])))
			
		if not os.path.exists(ppii_directory):
			print ppii_directory,"does not exist"
			print self.options["ppii_data_path"]
			sys.exit()
			
		
		for accession in os.listdir(ppii_directory):
			ppi_results_path = os.path.join(ppii_directory,accession,"ppIIpred.txt")
			ppi_results_out_path = os.path.join(self.options["ppii_data_path"],accession + ".json")
			if os.path.exists(ppi_results_path):
				scores = [float(x) for x in open(ppi_results_path).read().split()]
				data = {"ppii": scores}
				
				with open(ppi_results_out_path, 'w') as outfile:
					json.dump(data, outfile)
				
				if os.path.exists(ppi_results_out_path):
					print ppi_results_out_path
					
	def getPPII(self,accession):
		self.check_directory()
		
		self.options["ss_data_path"] = os.path.join(self.options["data_path"],"ss")
		
		if not os.path.exists(os.path.join(self.options["ss_data_path"])):
			os.mkdir(os.path.join(os.path.join(self.options["ss_data_path"])))
		
		self.options["ppii_data_path"] = os.path.join(self.options["ss_data_path"],"ppii")
		
		out_path = os.path.join(self.options["ppii_data_path"],accession + ".json")
	
		if os.path.exists(out_path):
			return json.loads(open(out_path).read())
		else:
			return {}
		
		
	##------------------------------------------------------------------##
	def getFellsUniprot(self,accession):
		self.check_directory()
		
		url = "http://www.uniprot.org/uniprot/" + accession + ".fasta"
		out_path = os.path.join(self.options["data_path"] , "uniprot",accession + ".fasta")
	
		if os.path.exists(out_path):
			if (time.time() - os.path.getctime(out_path))/60/60/24 > self.options["remake_age"] :
				os.remove(out_path)
	
		if not os.path.exists(out_path):
			try:
				cmd = "wget -O "+ out_path + " '"+url+"'"
				#print "-"*50
				print cmd
				time.sleep(self.options["wait_time"])
				os.system(cmd)
				#print "-"*50
			
			except Exception,e:
				print e
		
		sequence = "".join(open(out_path).read().split("\n")[1:])
		
		return self.getFells(sequence)
		
	##------------------------------------------------------------------##

	def getFells(self,sequence):
		
		self.options["ss_data_path"] = os.path.join(self.options["data_path"],"ss")
		
		if not os.path.exists(os.path.join(self.options["ss_data_path"])):
			os.mkdir(os.path.join(os.path.join(self.options["ss_data_path"])))
		
		self.options["fells_data_path"] = os.path.join(self.options["ss_data_path"],"fells")
		if not os.path.exists(os.path.join(self.options["fells_data_path"])):
			os.mkdir(os.path.join(os.path.join(self.options["fells_data_path"])))
			
			
		id_hash = md5.new(sequence).hexdigest()
		out_path = os.path.join(self.options["fells_data_path"], id_hash + ".json")
		
		if os.path.exists(out_path):
			if (time.time() - os.path.getctime(out_path))/60/60/24 > self.options["remake_age"] :
				os.remove(out_path)
	
		if not os.path.exists(out_path):
			try:
				import requests
				session=requests.Session()
				
				#========================================================================
				
				submit_url = "http://protein.bio.unipd.it/fellsws/submit"
				
				post_data={'sequence':">MySEQ\n" + sequence}
				post_response=session.post(url=submit_url, data=post_data)
				
				#========================================================================
				
				status_url = "http://protein.bio.unipd.it/fellsws/status/" + json.loads(post_response.content)["jobid"]
				get_response=session.get(url=status_url)
				
				running = True
				iteration = 0
				print "Running:",
				while running:
					if get_response.status_code == 200:
						if json.loads(get_response.content)["status"] == "done":
							running = False
						else:
							time.sleep(3)
							iteration += 1
							print "-\r"
							get_response=session.get(url=status_url)
				
				results_id = json.loads(get_response.content)["names"][0][1]
			
				#========================================================================
				
				result_url = "http://protein.bio.unipd.it/fellsws/result/" + results_id
				get_response=session.get(url=result_url)
				open(out_path,"w").write(get_response.content)
				
				#========================================================================
				
			except Exception,e:
				print e
				raise
		
		return json.loads(open(out_path).read())
	
	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##
	
	
if __name__ == "__main__":
	
	
	dataDownloaderObj = ssDownloader()
	dataDownloaderObj.options["data_path"] = "../../../../data"
	
	#dataDownloaderObj.getFells("MEEPQSDPSVEPPLSQETFSDLWKLLPENNVLSPLPSQAMDDLMLSPDDIEQWFTEDPGPDEAPRMPEAAPPVAPAPAAPTPAAPAPAPSWPLSSSVPSQKTYQGSYGFRLGFLHSGTAKSVTCTYSPALNKMFCQLAKTCPVQLWVDSTPPPGTRVRAMAIYKQSQHMTEVVRRCPHHERCSDSDGLAPPQHLIRVEGNLRVEYLDDRNTFRHSVVVPYEPPEVGSDCTTIHYNYMCNSSCMGGMNRRPILTIITLEDSSGNLLGRNSFEVRVCACPGRDRRTEEENLRKKGEPHHELPPGSTKRALPNNTSSSPQPKKKPLDGEYFTLQIRGRERFEMFRELNEALELKDAQAGKEPGGSRAHSSHLKSKKGQSTSRHKKLMFKTEGPDSD")
	#dataDownloaderObj.getFellsUniprot("P04637")
	#dataDownloaderObj.getFellsUniprot("P06400")
	#print dataDownloaderObj.movePPII('/Users/normandavey/Downloads/Norman 3/RunNN_Proteome_normal/results/all/all/')
	#print dataDownloaderObj.getPPII("P04637")
	