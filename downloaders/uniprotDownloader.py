import os
import re
import time
import string
import json
import requests
import pprint
from os import mkdir
from os import remove
from os.path import join
from os.path import exists
from os.path import getctime
from xml.dom import minidom
from xml.etree import cElementTree as elementtree


class uniprotDownloader():
    def __init__(self):
        self.options = {}
        self.options["wait_time"] = 0.01
        self.options["data_path"] = ""
        self.options["remake_age"] = 90
        self.settings = {}
        self.settings["disease_keywords"] = ["KW-0014","KW-0015","KW-0020","KW-0023","KW-0026","KW-0038","KW-0043",
            "KW-0065","KW-0069","KW-0070","KW-0087","KW-0122","KW-0161","KW-0172","KW-0182","KW-0192","KW-0199",
            "KW-0209","KW-0214","KW-0218","KW-0219","KW-0225","KW-0241","KW-0242","KW-0248","KW-0263","KW-0307",
            "KW-0316","KW-0322","KW-0331","KW-0335","KW-0355","KW-0360","KW-0361","KW-0362","KW-0367","KW-0370",
            "KW-0380","KW-0435","KW-0451","KW-0454","KW-0461","KW-0466","KW-0510","KW-0523","KW-0550","KW-0553",
            "KW-0586","KW-0622","KW-0656","KW-0657","KW-0682","KW-0685","KW-0705","KW-0751","KW-0757","KW-0772",
            "KW-0792","KW-0821","KW-0852","KW-0855","KW-0856","KW-0857","KW-0887","KW-0898","KW-0900","KW-0901",
            "KW-0905","KW-0907","KW-0908","KW-0910","KW-0912","KW-0913","KW-0923","KW-0940","KW-0942","KW-0947",
            "KW-0948","KW-0951","KW-0954","KW-0955","KW-0956","KW-0958","KW-0976","KW-0977","KW-0982","KW-0983",
            "KW-0984","KW-0985","KW-0986","KW-0987","KW-0988","KW-0989","KW-0991","KW-0992","KW-0993","KW-1004",
            "KW-1007","KW-1008","KW-1011","KW-1013","KW-1014","KW-1016","KW-1020","KW-1021","KW-1022","KW-1023",
            "KW-1024","KW-1026","KW-1054","KW-1056","KW-1057","KW-1058","KW-1059","KW-1060","KW-1062","KW-1063",
            "KW-1065","KW-1066","KW-1067","KW-1068","KW-1186","KW-1211","KW-1212","KW-1215","KW-1268","KW-1274"]
    
    def check_directory(self):
        data_path = self.options["data_path"]
        uniprot_path = join(data_path, "uniprot") 
        if not exists(uniprot_path):
            mkdir(uniprot_path)

    def remove_if_old(self, file, max_living_time, force = False):
        if exists(file):
            max_living_time = 60*60*24*max_living_time
            living_time = time.time() - getctime(file)
            if (living_time > max_living_time) or force:
                remove(file)

    def _get_base_name(self, accession, file_type):
        if file_type.lower() == "humsavar":
            return "humsavar.txt"
        if file_type.lower() == "xml":
            return "{}.{}".format(accession, file_type.lower())
        if file_type.lower() == "fasta":
            return "{}.{}".format(accession, file_type.lower())
        if file_type.lower() == "pfam":
            return  "{}.pfam.xml".format(accession)
        if file_type.lower() == "mobi":
            return  "{}.mobidb.json".format(accession)
        
    def grabHumsavar(self):
        self.check_directory()
        self.grabUniProtFile("","humsavar",False)
        url = "ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/variants/humsavar.txt"
        out_path = os.path.join(self.options["data_path"], "uniprot","humsavar.txt")
    
        if os.path.exists(out_path):
            if (time.time() - os.path.getctime(out_path))/60/60/24 > self.options["remake_age"] :
                os.remove(out_path)
    
        if not os.path.exists(out_path):
            try:
                cmd = "wget -O "+ out_path + " '"+url+"'"
                #print "-"*50
                #print cmd
                time.sleep(self.options["wait_time"])
                os.system(cmd)
                #print "-"*50
            
            except Exception,e:
                print e
    
    def parseHumsavar(self,accession):
        self.check_directory()
        
        self.grabHumsavar()
        out_path = os.path.join(self.options["data_path"], "uniprot","humsavar.txt")
        
        mutations = {"mutations":{},"positions":{},"phenotype":{}}
        
        with open(out_path) as fileobject:
            for line in fileobject:
                line_bits = line.split()
                if len(line_bits) > 2:
                    if line_bits[1] == accession:
                        wildtype = line_bits[3][2:5]
                        mutant = line_bits[3][-3:]
                        position = line_bits[3][5:-3]
                        
                        
                        mutations["mutations"][line_bits[2]] = {
                        "position":position,
                        "wildtype":wildtype,
                        "mutant":position,
                        "FTId": line_bits[2],
                        "dbSNP": line_bits[5],
                        "variant_type": line_bits[4],
                        "variant": line_bits[3],
                        "description":" ".join(line_bits[6:])
                        }
                        
                        if line_bits[4] not in mutations["phenotype"]:
                            mutations["phenotype"][line_bits[4]] = {}
                        
                        if position not in mutations["phenotype"][line_bits[4]]:
                            mutations["phenotype"][line_bits[4]][position] = []
                            
                        mutations["phenotype"][line_bits[4]][position].append(line_bits[2])
                        
                        if position not in mutations["positions"]:
                            mutations["positions"][position] = []
                            
                        mutations["positions"][position].append(line_bits[2])
                            
        return mutations
    
    def grabAttributes(self,accession):
        self.check_directory()
        
        url = "http://slim.ucd.ie/rest/attributes/?uniprot_acc=" + accession + "&type=Conservation&type=IUPred&type=Anchor&type=PsiPred"
        
        out_path = os.path.join(self.options["data_path"], "uniprot",accession + ".attributes.json")
        
        
        if os.path.exists(out_path):
            if (time.time() - os.path.getctime(out_path))/60/60/24 > self.options["remake_age"] :
                os.remove(out_path)
        
            statinfo = os.stat(out_path)
            if statinfo.st_size == 0:
                os.remove(out_path)

        if not os.path.exists(out_path):
            try:
                cmd = "wget -T10 -O "+ out_path + " '"+url+"'"
                #print "-"*50
                #print cmd
                time.sleep(self.options["wait_time"])
                os.system(cmd)
                #print "-"*50
            
            except Exception,e:
                print e
                
    def parseAttributes(self,accession):
        try:
            self.grabAttributes(accession)
        
            out_path = os.path.join(self.options["data_path"], "uniprot",accession + ".attributes.json")
            with open(out_path) as outfile:
                data = json.load(outfile)
                return {"status":"Added","data":data}
        except Exception,e:
            return {"status":"Error","error_type":str(e)}

    def grabUniProtGenetree(self,accession):
        url = "https://www.uniprot.org/uniprot/?sort=score&desc=&compress=no&query=" + accession + "&fil=&limit=1&force=no&preview=true&format=tab&columns=id,database(GeneTree)"
        out_path = os.path.join(self.options["data_path"] , "uniprot",accession + ".genetree")
    
        if os.path.exists(out_path):
            if (time.time() - os.path.getctime(out_path))/60/60/24 > self.options["remake_age"] :
                os.remove(out_path)
    
        if not os.path.exists(out_path):
            try:
                cmd = "wget -O "+ out_path + " '"+url+"'"
                #print "-"*50
                #print cmd
                time.sleep(self.options["wait_time"])
                os.system(cmd)
                #print "-"*50
            
            except Exception,e:
                print e
    
    def getUniProtGenetree(self,accession):
        self.grabUniProtGenetree(accession)
        
        out_path = os.path.join(self.options["data_path"] , "uniprot",accession + ".genetree")
        
        if os.path.exists(out_path):
            line_bits = open(out_path).read().split("\n")
            if len(line_bits) > 1:
                return line_bits[1].split("\t")[1].strip(";")
            else:
                return ""
    
    def getUniProtGenetreeRecursive(self,accession):
        homologue_accessions = self.getUniProtUniref(accession.split("-")[0])
        genetree_accession = ""
        for homologue_accession in homologue_accessions:
            if homologue_accession[0:3] != "UPI":
                genetree_accession = self.getUniProtGenetree(homologue_accession)
                
            if genetree_accession != "":
                    break
                    
        return genetree_accession
        
    def grabUniProtUniref(self,accession):
        url = "https://www.uniprot.org/uniref/?sort=score&desc=&compress=no&query=uniprot:" + accession + "%20AND%20identity:0.5&fil=&limit=1&force=no&preview=true&format=tab&columns=id,members"
        out_path = os.path.join(self.options["data_path"] , "uniprot",accession + ".uniref")
    
        if os.path.exists(out_path):
            if (time.time() - os.path.getctime(out_path))/60/60/24 > self.options["remake_age"] :
                os.remove(out_path)
    
        if not os.path.exists(out_path):
            try:
                cmd = "wget -O "+ out_path + " '"+url+"'"
                #print "-"*50
                #print cmd
                time.sleep(self.options["wait_time"])
                os.system(cmd)
                #print "-"*50
            
            except Exception,e:
                print e
    
    def getUniProtUniref(self,accession):
        self.grabUniProtUniref(accession)
        
        out_path = os.path.join(self.options["data_path"] , "uniprot",accession + ".uniref")
        
        if os.path.exists(out_path):
            line_bits = open(out_path).read().split("\n")
            if len(line_bits) > 1:
                return line_bits[1].split("\t")[1].split("; ")
            else:
                return []
                
    def grabMobiDB(self,accession,force=False):
        self.grabUniProtFile(accession, "mobi", force)
        self.check_directory()
        
        url = "http://mobidb.bio.unipd.it/ws/entries/" + accession + "/disorder" 
        out_path = os.path.join(self.options["data_path"], "uniprot",accession + ".mobidb.json")
        
        if os.path.exists(out_path):
            if (time.time() - os.path.getctime(out_path))/60/60/24 > self.options["remake_age"] or force:
                os.remove(out_path)
        
        if not os.path.exists(out_path):
            try:
                cmd = "wget -T10 -O "+ out_path + " '"+url+"'"
                #print "-"*50
                #print cmd
                time.sleep(self.options["wait_time"])
                os.system(cmd)
                #print "-"*50
            
            except Exception,e:
                print e

    def parseMobiDB(self,accession):
        try:
            self.grabMobiDB(accession)
        
            out_path = os.path.join(self.options["data_path"], "uniprot",accession + ".mobidb.json")
            with open(out_path) as outfile:
                return {"status":"Added","data":json.load(outfile)}
                
        except Exception,e:
            return {"status":"Error","error_type":str(e)}
    
    def get_uniprot_accession_taxa(self,taxon_id,format,reviewed=False):
        self.check_directory()
        
        url = "https://www.uniprot.org/uniprot/?query=reviewed:yes%20taxonomy:" + taxon_id + "&format=" + format 
        out_path = os.path.join(self.options["data_path"], "uniprot",taxon_id + ".unreviewed." + format)
        
        if reviewed:
            url += "&fil=reviewed%3Ayes"
            out_path = os.path.join(self.options["data_path"], "uniprot",taxon_id + "." +  format)
        
        
        if os.path.exists(out_path):
            if (time.time() - os.path.getctime(out_path))/60/60/24 > self.options["remake_age"]:
                os.remove(out_path)

        if os.path.exists(out_path):
            if os.stat(out_path).st_size == 0:
                os.remove(out_path)
                
        if not os.path.exists(out_path):
            try:
                cmd = "wget -O "+ out_path  + " '"+url+"'"
                time.sleep(self.options["wait_time"])
                os.system(cmd)
            except Exception,e:
                print e

    def get_uniprot_accession_list(self,accessions,format):
        import urllib,urllib2
        url = 'https://www.uniprot.org/uploadlists/'
        accessions.sort()
        # hash_accession = ";".join(accessions)
        # hash = md5.new(hash_accession).hexdigest()
        # out_path = os.path.join(self.options["data_path"], "uniprot",hash + ".list." + format)
        params = {
            'from':'ACC',
            'to':'ACC',
            'format':format,
            'query':" ".join(accessions)
        }
        data = urllib.urlencode(params)
        request = urllib2.Request(url, data)
        contact = "" # Please set your email address here to help us debug in case of problems.
        request.add_header('User-Agent', 'Python %s' % contact)
        response = urllib2.urlopen(request)
        page = response.read()
        open("test.xml","w").write(page)

    def parse_uniprot_accession_taxa(self,taxon_id,reviewed=False):
        self.get_uniprot_accession_taxa(taxon_id,"list",reviewed=reviewed)
        out_path = os.path.join(self.options["data_path"], "uniprot",taxon_id + ".unreviewed.list" )
        
        if os.path.exists(out_path):
            return open(out_path).read().split()
        else:
            return []
    
    def grabUniProtPfam(self,accession,force=False):
        self.grabUniProtFile(accession,"pfam",force)

    def parseUniProtPfam(self,accession):
        self.grabUniProtPfam(accession)
        
        xml_path = os.path.join(self.options["data_path"], "uniprot",accession + ".pfam.xml")
        
        domains_data = {}
        error_pattern = re.compile("<error>.+</error>")
        
        try:
            if os.path.exists(xml_path):
                
                xml = open(xml_path).read()
                
                if len(open(xml_path).read()) == 0:
                    return  {"status":"Error","error_type":"no data returned"}
            
                tree = elementtree.parse(xml_path)
                root = tree.getroot()
                
                for entry in tree.iter('{https://pfam.xfam.org/}entry'):
                    sequence_tag = entry.find('{https://pfam.xfam.org/}sequence')
                    sequence = sequence_tag.text
                    for domain in tree.iter('{https://pfam.xfam.org/}match'):
                        pfam_accession = domain.attrib['accession']
                        pfam_id = domain.attrib['id']
                        pfam_type = domain.attrib['type']
                        
                        domains_data[pfam_accession] = {"pfam_accession":pfam_accession,"pfam_id":pfam_id,"pfam_type":pfam_type,"matches":[]}
                    
                        for domain_instance in domain.iter('{https://pfam.xfam.org/}location'):
                            start = domain_instance.attrib['start']
                            end = domain_instance.attrib['end']
                            evalue = domain_instance.attrib['evalue']
                            domains_data[pfam_accession]["matches"].append({"start":start,"end":end,"evalue":evalue,'sequence':sequence[int(start)-1:int(end)]})
                
                return {"status":"Added","data":domains_data}
            else:
                return {"status":"Error","error_type":"File not found"}
                
        except Exception,e:
            return {"status":"Error","error_type":str(e)}
    
    def _get_url(self, file_type, accession=None):
        if (file_type.lower() == "xml") or (file_type.lower() == "fasta"):
            return "https://www.uniprot.org/uniprot/{}.{}".format(accession, file_type.lower())
        if file_type.lower() == "humsavar":
            return "ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/variants/humsavar.txt"
        if file_type.lower() == "pfam":
            return "http://pfam.xfam.org/protein/" + accession + "?output=xml"
        if file_type.lower() == "mobi":
            return "http://mobidb.bio.unipd.it/ws/entries/" + accession + "/disorder" 

    def grabUniProtFile(self, accession, file_type, force):
        self.check_directory()
        url = self._get_url(file_type, accession)
        base_name = self._get_base_name(accession, file_type)
        out_path = join(self.options["data_path"] , "uniprot", base_name)
        self.remove_if_old(out_path, self.options["remake_age"], force)
        if not exists(out_path):
            time.sleep(self.options["wait_time"])
            response = requests.get(url, timeout=10)
            up = response.text
            with open(out_path, 'w') as fh:
                fh.write(up)
        return out_path

    def grabUniProtFasta(self, accession, force=False):
        self.grabUniProtFile(accession, "fasta", force)
    
    def parseUniProtFasta(self,accession):
        out_path = os.path.join(self.options["data_path"] , "uniprot",accession + ".fasta")
        protein_data = {}
        gnRe = re.compile(r"GN=\S+")

        if not os.path.exists(out_path):
            self.grabUniProtFasta(accession)
            
        if os.path.exists(out_path):
            try:
                line_bits = open(out_path).read().split("\n")
        
                sequence = "".join(line_bits[1:])
        
                accessions = line_bits[0].split()[0].split("|")
                
                if len(accessions) > 1:
                    accession = accessions[1]
                    identifier = accessions[2]
    
                    fullname = " ".join(line_bits[0].split("OS=")[0].split()[1:])
                    gene = ""
                    geneBits = gnRe.findall(line_bits[0])
                    if len(geneBits) > 0:
                        gene = geneBits[0][3:]
                else:
                    accession = line_bits[0][1:].split()[0]
                    identifier = accession
                    gene = accession
                    fullname = accession
                    
                protein_data = {
                    "identifier":identifier,
                    "gene":gene,
                    "fullname":fullname,
                    "sequence":sequence
                    }
            except:
                print out_path
                raise
                
        return protein_data
    
    def grabUniProt(self, accession, force=False):
        return self.grabUniProtFile(accession, "xml", force)
    
    def parseGO(self,accession,parse_keywords=True,generic=False):
        data = self.parseUniProt(accession,parse_keywords=True,parse_generic=True)
    
        output = {"go":{}}
        
        if 'data' in data:
            if 'GO' in data['data']:
                output['go'] = data['data']['GO']
                
        return output

    def parseLocalisation(self,accession,parse_keywords=True,generic=False):
        data = self.parseUniProt(accession,parse_keywords=True,parse_generic=True)
        
        output = {}
        
        if 'data' in data:
            localisation = {}
            if 'localisation' in data['data']:
                localisation["localisation_keyword"] = data['data']['localisation']
                
                if data['data']['localisation_isoform'] != {}:
                    localisation["localisation_isoform"] = data['data']['localisation_isoform']
                
                
                localisation["basic_localisation"] = []
                
                for subcellular_compartment in ['Nucleus','Cytoplasm','Cell membrane']:
                    if subcellular_compartment in localisation["localisation_keyword"]:
                        localisation["basic_localisation"].append(subcellular_compartment)
                
            if 'keywords' in data['data']:
                if 'Transmembrane helix' in data['data']['keywords'] or 'Transmembrane' in data['data']['keywords']:
                    localisation["basic_localisation"].append('Transmembrane')
                    localisation["Transmembrane"] = True
        
            
            for header in ['id','protein_name','family','gene_name']:
                if header in data['data']:
                    output[header] = data['data'][header]
            
            localisation["localisation_go"] = {}
            if 'GO' in data['data']:
                if 'C' in data['data']['GO']:
                    for compartment in data['data']['GO']['C']:
                        localisation["localisation_go"][compartment['term']] = compartment
            
            output = {"localisation":localisation}
        
        return output
    
    def parseKeywords(self,accession,generic=False):
        return self.parseUniProt(accession,parse_keywords=True,parse_generic=generic)
        
    def parseIsoforms(self,accession,generic=False):
        return self.parseUniProt(accession,parse_isoforms=True,parse_generic=generic)
        
    def parseSNPs(self,accession,generic=False):
        return self.parseUniProt(accession,parse_snps=True,parse_generic=generic)
    
    def parseSequence(self,accession):
        return self.parseUniProt(accession,parse_sequence=True)
    
    def parseFeatures(self,accession,generic=False):
        return self.parseUniProt(accession,parse_features=True,parse_generic=generic)
    
    def parseDomains(self,accession,generic=True):
        data = self.parseUniProt(accession, parse_generic=generic)
        result = {"status": "Success" if "data" in data else "Error",
                "data": data.get("data",{}).get("Pfam",{})}
        return result
        
    def parsePDBs(self,accession):
        data = self.parseUniProt(accession,parse_generic=True)
        
        if 'data' in data:
            if 'PDB' in data['data']:
                return {"status":"Success",'data':data['data']['PDB']}
            else: return {"status":"Success",'data':{}}
        else: return data
        
    def parseBasic(self,accession):
        data = self.parseUniProt(accession,parse_generic=True)
        basic_data_types = ["accession","description","family",'gene_name','gene_names','id','protein_name','protein_names','sequence','species_common','species_scientific','taxon_id','taxonomy','version']
        basic_data = {}
        if 'data' in data:
            for data_type in basic_data_types:
                if data_type in data['data']:	
                    basic_data[data_type] =  data['data'][data_type]
        
            return {"status":"Success",'data':basic_data}
        else: return {"status":"Error",'data':{}}
        
    def parseSecondaryAccessions(self,accession):
        data = self.parseUniProt(accession,parse_generic=True)
        
        if 'data' in data:
            if "secondary_accessions" in data['data']:	
                return {"status":"Success",'data':{"secondary_accessions":data['data']["secondary_accessions"]}}
            else: 
                return {"status":"Error",'data':{}}
        else: return {"status":"Error",'data':{}}
        
    def parseUniProt(self,accession,parse_features=False,parse_keywords=False,parse_snps=False,parse_attributes=False,parse_disorder=False,parse_isoforms=False,parse_sequence=False,parse_generic=True,force=False):
        self.grabUniProt(accession,force)
    
        proteins_data = {
            "accession":accession,
            "isoform":False
            }
          
        if len(accession.split("-")) > 1:
            proteins_data["isoform"] = True  
            print "ISOFORM"
            
        xml_path = os.path.join(self.options["data_path"] , "uniprot", accession + ".xml")
        
        error_pattern = re.compile("<error>.+</error>")

        try:
            if os.path.exists(xml_path):
                
                if len(open(xml_path).read()) == 0:
                    return  {"status":"Error","error_type":"no data returned"}
            
                tree = elementtree.parse(xml_path)
                root = tree.getroot()
                
                for entry in tree.iter('{http://uniprot.org/uniprot}entry'):
                    #----
                    proteins_data["PDB"] = {}
                    references = {}
                    for reference in tree.iter('{http://uniprot.org/uniprot}evidence'):
                        for dbReference in reference.iter('{http://uniprot.org/uniprot}dbReference'):
                            if dbReference.attrib['type'] == 'PubMed':
                                references[reference.attrib['key']] = dbReference.attrib['id']
                                
                    #----
                    
                    if parse_generic:
                        try:
                            proteins_data["accession"] = entry.find('{http://uniprot.org/uniprot}accession').text#version
                        except:
                            proteins_data["accession"] = ""
                        
                        try:
                            proteins_data["secondary_accessions"] = []
                            for accession in tree.iter('{http://uniprot.org/uniprot}accession'):
                                proteins_data["secondary_accessions"].append(accession.text)
                        except:
                                proteins_data["secondary_accessions"] = []
                        
                        try:
                            proteins_data["id"] = entry.find('{http://uniprot.org/uniprot}name').text#version
                        except:
                            proteins_data["id"] = ""
                
                        try:
                            proteins_data["fragment"] = False
                            
                            if "fragment" in entry.find('{http://uniprot.org/uniprot}sequence').attrib:
                                proteins_data["fragment"] = True
                        except:
                            proteins_data["fragment"] = False
                                           
                        try:
                            proteins_data["version"] = entry.attrib['version'] #version
                        except:
                            proteins_data["version"] = ""
                
                        try:
                            proteins_data["sequence"] = entry.find('{http://uniprot.org/uniprot}sequence').text.replace("\n","")
                            if parse_sequence:
                                return proteins_data["sequence"]
                        except:
                            proteins_data["sequence"] = ""
            
                        try:
                            proteins_data["protein_name"] = entry.find('.//{http://uniprot.org/uniprot}recommendedName/{http://uniprot.org/uniprot}fullName').text
                        except:
                            proteins_data["protein_name"] = ""
            
                        try:
                            proteins_data["protein_names"] = []
                            for protein_name in entry.findall('.//{http://uniprot.org/uniprot}alternativeName/{http://uniprot.org/uniprot}fullName'):
                                proteins_data["protein_names"].append(protein_name.text)
                        except:
                            proteins_data["protein_names"] = []
                            
                        if proteins_data["protein_name"] == "":
                            try:
                                proteins_data["protein_name"] = entry.find('.//{http://uniprot.org/uniprot}submittedName/{http://uniprot.org/uniprot}fullName').text
                            except:
                                proteins_data["protein_name"] = ""
                
                        try:
                            proteins_data["gene_name"] = entry.find('.//{http://uniprot.org/uniprot}gene/{http://uniprot.org/uniprot}name').text
                        except:
                            proteins_data["gene_name"] = ""
            
                        try:
                            proteins_data["gene_names"] = []
                            for gene_name in entry.findall('.//{http://uniprot.org/uniprot}gene/{http://uniprot.org/uniprot}name'):
                                proteins_data["gene_names"].append(gene_name.text)
                        except:
                            proteins_data["gene_names"] = []
                            
                        try:
                            proteins_data["species_common"] = entry.find("{http://uniprot.org/uniprot}organism/{http://uniprot.org/uniprot}name[@type='common']").text#http://uniprot.org/uniprot}organism/{http://uniprot.org/uniprot}name").text
                        except:
                            proteins_data["species_common"] = ""
                        
                        try:
                            proteins_data["species_scientific"] = entry.find("{http://uniprot.org/uniprot}organism/{http://uniprot.org/uniprot}name[@type='scientific']").text.split("(")[0]#http://uniprot.org/uniprot}organism/{http://uniprot.org/uniprot}name").text
                        except:
                            proteins_data["species_scientific"] = ""
                        
                        try:
                            proteins_data["taxon_id"] = entry.find('{http://uniprot.org/uniprot}organism/{http://uniprot.org/uniprot}dbReference').attrib['id']
                        except:
                            proteins_data["taxon_id"] = ""
            
                        try:
                            proteins_data["description"] = entry.find("{http://uniprot.org/uniprot}comment[@type='function']/{http://uniprot.org/uniprot}text").text
                        except:
                            proteins_data["description"] = ""
                        
                        try:
                            proteins_data["family"] = ""
                            for comment in entry.findall("{http://uniprot.org/uniprot}comment[@type='similarity']/{http://uniprot.org/uniprot}text"):
                                if comment.text.replace('.','').split()[-1].replace('.','') == "family":
                                    proteins_data["family"] = comment.text.replace('.','').replace('Belongs to the ','')
                        except:
                            proteins_data["family"] = ""
                        
                        proteins_data["taxonomy"] = []
                    
                        for elem in tree.iter('{http://uniprot.org/uniprot}taxon'):
                            proteins_data["taxonomy"].append(elem.text)
                    
                        proteins_data["taxonomy"] = "|".join(proteins_data["taxonomy"])
                    
                    if proteins_data["isoform"]:
                        self.grabUniProtFasta(proteins_data["accession"])
                        fasta_data = self.parseUniProtFasta(proteins_data["accession"])
                        
                        if "sequence" in fasta_data:
                            proteins_data["sequence"] = fasta_data["sequence"] 
                        
                        isoform_names = []
                        for isoform in tree.iter('{http://uniprot.org/uniprot}isoform'):
                            
                            if isoform.find('{http://uniprot.org/uniprot}id').text == proteins_data["accession"]:
                                # isoform.find('{http://uniprot.org/uniprot}sequence').attrib["type"]
                                # isoform.find('{http://uniprot.org/uniprot}sequence').attrib["ref"]
                            
                                for name in isoform.iter('{http://uniprot.org/uniprot}name'):
                                    isoform_names.append(name.text)
                        
                                proteins_data["isoform_names"] = isoform_names
                            
                                if len(isoform_names) > 1:
                                    proteins_data["protein_name"] = proteins_data["protein_name"] + " - Isoform " + "/".join(isoform_names[1:])
                                elif len(isoform_names) == 1:
                                    proteins_data["protein_name"] = proteins_data["protein_name"] + " - Isoform " + isoform_names[0]
                                else:
                                    proteins_data["protein_name"] = fasta_data["fullname"] 
                    
                
                                
                    if not proteins_data["isoform"] and parse_generic:
                        proteins_data["Pfam"] = []
                        proteins_data["GO"] = {}
                        for dbReference in tree.iter('{http://uniprot.org/uniprot}dbReference'):
                            if dbReference.attrib["type"] == "PDB":
                                for dbReferenceProperty in dbReference.iter('{http://uniprot.org/uniprot}property'):
                                    if dbReferenceProperty.attrib["type"] == "chains":
                                        proteins_data["PDB"][dbReference.attrib["id"]] = {}
                
                                        for chain in dbReferenceProperty.attrib['value'].split(","):
                                            try:
                                                chainBits = chain.split("=")
                                                chainIds = chainBits[0].strip()
                            
                                                for chainId in chainIds.split("/"):
                                                    start = int(chainBits[1].split("-")[0])
                                                    end = int(chainBits[1].split("-")[1].strip(" ."))
                                                    proteins_data["PDB"][dbReference.attrib["id"]][chainId] = {"start":start,"end":end}
                                            except:
                                                pass
                            elif dbReference.attrib["type"] == "Pfam":
                                proteins_data["Pfam"].append(dbReference.attrib["id"])
                            
                            elif dbReference.attrib["type"] == "GeneTree":
                                proteins_data["GeneTree"] = dbReference.attrib["id"]

                            elif dbReference.attrib["type"] == "GO":
                                tmp = {"id":dbReference.attrib["id"]}
                                go_class = ""
                                for dbReferenceProperty in dbReference.iter('{http://uniprot.org/uniprot}property'):
                                    if dbReferenceProperty.attrib["type"] == "term":
                                        go_class =  dbReferenceProperty.attrib["value"].split(':')[0]
                                        go_term_name =  dbReferenceProperty.attrib["value"].split(':')[1]
                                        
                                        tmp['term'] = go_term_name
                                        tmp['class'] = go_class
                                    else:
                                        tmp[dbReferenceProperty.attrib["type"]] = dbReferenceProperty.attrib["value"] 
                                if go_class not in proteins_data["GO"]:
                                    proteins_data["GO"][go_class] = []
                                proteins_data["GO"][go_class].append(tmp)
                                
                    #####
                    #####
                    #####
                    
                    if parse_isoforms:
                        isoform_pattern = re.compile("[iI]soform [0-9]+")
                        proteins_data["isoforms"] = {
                        "alternative_exons":[],
                        "isoform_details":{},
                        }
                        
                        for isoform in tree.iter('{http://uniprot.org/uniprot}isoform'):
                            isoform_id = isoform.find('{http://uniprot.org/uniprot}id').text 
                            
                            proteins_data["isoforms"]["isoform_details"][isoform_id] = {
                            "id":isoform_id,
                            "names":[],
                            "sequence_type":"",
                            "sequence_ref":"",
                            "description":""
                            }
                            
                            for name in isoform.iter('{http://uniprot.org/uniprot}name'):
                                proteins_data["isoforms"]["isoform_details"][isoform_id]["names"].append(name.text)
                                
                                isoform_sequence = isoform.find('{http://uniprot.org/uniprot}sequence')
                                if isoform_sequence != None:
                                    if 'type' in isoform_sequence.attrib: proteins_data["isoforms"]["isoform_details"][isoform_id]["sequence_type"] = isoform_sequence.attrib['type']
                                    if 'ref' in isoform_sequence.attrib: proteins_data["isoforms"]["isoform_details"][isoform_id]["sequence_ref"] = isoform_sequence.attrib['ref']
                                
                                isoform_text = isoform.find('{http://uniprot.org/uniprot}text')
                                if isoform_text != None:
                                    proteins_data["isoforms"]["isoform_details"][isoform_id]["description"] = isoform_text.text
                        
                        for variant in tree.iter("{http://uniprot.org/uniprot}feature"):
                            if variant.attrib['type'] == 'splice variant':
                                description = variant.attrib['description']
                                
                                location_begin_tag = variant.find('{http://uniprot.org/uniprot}location/{http://uniprot.org/uniprot}begin')
                                location_end_tag = variant.find('{http://uniprot.org/uniprot}location/{http://uniprot.org/uniprot}end')
                                location_position_tag = variant.find('{http://uniprot.org/uniprot}location/{http://uniprot.org/uniprot}position')
                                original_tag = variant.find('{http://uniprot.org/uniprot}original')
                                variation_tag = variant.find('{http://uniprot.org/uniprot}variation')
                                
                                start = None
                                end = None 
                                original = None
                                variation = None
                                
                                if location_begin_tag != None: start = location_begin_tag.attrib['position']
                                if location_end_tag != None: end = location_end_tag.attrib['position']
                                if location_position_tag != None: position = location_position_tag.attrib['position']
                                if original_tag != None: original = original_tag.text
                                if variation_tag != None: variation = variation_tag.text
                                
                                if location_position_tag != None:
                                    start = location_position_tag.attrib['position']
                                    end = location_position_tag.attrib['position']
                                    
                                isoform_ids = [proteins_data["accession"] + "-" + isoform_match.split()[1] for isoform_match in  isoform_pattern.findall(description)]
                                alternative_exon = {
                                "id": variant.attrib['id'],
                                "start": start ,
                                "end": end,
                                "original": original,
                                "variation": variation,
                                "removed": original == None and variation == None,
                                "description":description,
                                "isoform_ids":isoform_ids
                                }
                                proteins_data["isoforms"]["alternative_exons"].append(alternative_exon)
                                
                                
                                for isoform_id in isoform_ids:
                                    if isoform_id in proteins_data["isoforms"]["isoform_details"]:
                                        if 'variation' not in proteins_data["isoforms"]["isoform_details"][isoform_id ]:
                                            proteins_data["isoforms"]["isoform_details"][isoform_id]['variation' ] = []
                                        
                                        proteins_data["isoforms"]["isoform_details"][isoform_id]['variation'].append(alternative_exon)
                    
                    #####
                    #####
                    #####
                    
                    if parse_keywords:
                        proteins_data["keywords"] = {}
                        proteins_data["disease_keywords"] = {}
                        proteins_data["diseases"] = {}
                        proteins_data["localisation"] = []
                        proteins_data["localisation_isoform"] = {}

                        for keyword in tree.iter('{http://uniprot.org/uniprot}keyword'):
                            proteins_data["keywords"][keyword.text] = keyword.attrib['id']
                            if keyword.attrib['id'] in self.settings["disease_keywords"]:
                                proteins_data["disease_keywords"][keyword.text] = keyword.attrib['id']
                            
                        #for subcellularLocation in tree.iter('{http://uniprot.org/uniprot}subcellularLocation'):
                        
                        
                        for comment in tree.iter("{http://uniprot.org/uniprot}comment"):
                            if comment.attrib["type"] == "disease":
                                for disease in comment.findall('{http://uniprot.org/uniprot}disease'):
                                    disease_id = disease.attrib["id"]
                                    disease_name = disease.find('{http://uniprot.org/uniprot}name').text
                                    disease_acronym = disease.find('{http://uniprot.org/uniprot}acronym').text
                                    disease_description = disease.find('{http://uniprot.org/uniprot}description').text
                                    disease_db_reference = {"source":disease.find('{http://uniprot.org/uniprot}dbReference').attrib["type"],"id":disease.find('{http://uniprot.org/uniprot}dbReference').attrib["id"]}
                                    
                                    proteins_data["diseases"][disease_id]  = {
                                    "disease_name":disease_name,
                                    "disease_acronym":disease_acronym,
                                    "disease_description":disease_description,
                                    "disease_db_reference":disease_db_reference
                                    }
                                    
                            if comment.attrib["type"] == "subcellular location":
                                for subcellularLocation in comment.findall('{http://uniprot.org/uniprot}subcellularLocation'):
                                    location_tag = subcellularLocation.find('{http://uniprot.org/uniprot}location')
                                    topology_tag = subcellularLocation.find('{http://uniprot.org/uniprot}topology')
                                    orientation_tag = subcellularLocation.find('{http://uniprot.org/uniprot}orientation')
                                    molecule_tag = comment.find('{http://uniprot.org/uniprot}molecule')
                                    
                                    localisation = []
                                    location = ""
                                    topology = ""
                                    orientation = ""
                                
                                    if location_tag != None: 
                                        location = location_tag.text
                                        localisation.append(location)
                                        
                                    if topology_tag != None: 
                                        topology = topology_tag.text
                                        localisation.append(topology)
                                        
                                    if orientation_tag != None: 
                                        orientation = orientation_tag.text
                                        localisation.append(orientation)
    
                                    if len(localisation) > 1:
                                        if localisation[0] not in proteins_data["localisation"]:
                                            proteins_data["localisation"].append(localisation[0])
                                        
                                        if "|".join(localisation) not in proteins_data["localisation"]:
                                            proteins_data["localisation"].append("|".join(localisation))
                                    else:
                                        if "|".join(localisation) not in proteins_data["localisation"]:
                                            proteins_data["localisation"].append("|".join(localisation))
                                        
                                    if molecule_tag != None: 
                                        molecule = molecule_tag.text
                                        if molecule not in proteins_data["localisation_isoform"]:
                                            proteins_data["localisation_isoform"][molecule] = []
                                        
                                        localisation = "|".join(localisation)
                                        if localisation not in proteins_data["localisation"]:
                                            proteins_data["localisation_isoform"][molecule].append(localisation)
                        
                        if parse_isoforms:
                            pass
                    
                    if parse_features:
                        proteins_data["features"] = {}
                        
                        for feature in tree.iter('{http://uniprot.org/uniprot}feature'):
                            
                            pmid = []
                            if "evidence" in feature.attrib:
                                for evidence in feature.attrib["evidence"].split():
                                    if evidence in references:
                                        pmid.append(references[evidence])
                            
                                del feature.attrib["evidence"]
                            
                            if feature.attrib["type"] not in proteins_data["features"]:
                                proteins_data["features"][feature.attrib["type"]] = []
                        
                            start = feature.find('{http://uniprot.org/uniprot}location/{http://uniprot.org/uniprot}begin')
                            end = feature.find('{http://uniprot.org/uniprot}location/{http://uniprot.org/uniprot}end')
                            if start != None: 
                                feature.attrib['start'] = start.attrib['position']
                            else:
                                feature.attrib['start'] = feature.find('{http://uniprot.org/uniprot}location/{http://uniprot.org/uniprot}position').attrib['position']
                                
                            if end != None: 
                                feature.attrib['end'] = end.attrib['position']
                            else:
                                feature.attrib['end'] = feature.find('{http://uniprot.org/uniprot}location/{http://uniprot.org/uniprot}position').attrib['position']
                            
                            feature.attrib['pmid'] = pmid
                            proteins_data["features"][feature.attrib["type"]].append(feature.attrib)
                
                #####
                #####
                #####
                
                if parse_snps:
                    proteins_data["mutations"] = self.parseHumsavar(proteins_data["accession"])
                    proteins_data['disease_mutations'] = None
                    
                    if 'phenotype' in proteins_data["mutations"]:
                        if 'Disease' in proteins_data["mutations"]['phenotype']:
                            proteins_data['disease_mutations'] = proteins_data["mutations"]['phenotype']['Disease']
                    
                if parse_attributes:
                    response = {}# self.parseAttributes(proteins_data["accession"])
                    proteins_data['disorder'] = None
                    proteins_data['conservation'] = None
                    proteins_data['anchor'] = None
                
                    if 'data' in response:
                        if 'IUPred' in  response['data']:
                            proteins_data['disorder'] = response['data'][u'IUPred']
                        
                        if 'Conservation' in  response['data']:
                            proteins_data['conservation'] = response['data'][u'Conservation']
                        
                        if 'Anchor' in  response['data']:
                            proteins_data['anchor'] = response['data'][u'Anchor']
                    
                if parse_disorder:
                    response= self.parseMobiDB(proteins_data["accession"])
                    proteins_data['disprot_consensus'] = None
                    proteins_data['disprot'] = None
                    
                    if 'data' in response:
                        if 'consensus' in response['data']:
                            proteins_data['disorder_consensus'] = response['data']['consensus'][u'full']
                        
                            if u'disprot' in response['data']:
                                proteins_data['disprot']  = response['data']['consensus'][u'disprot']
                
                return {"status":"Success","data":proteins_data}
            else:
                return {"status":"Error","error_type":"File not found"}
            
        except Exception,e:
            print "Error",xml_path
            return {"status":"Error","error_type":str(e)}
    
if __name__ == "__main__":
    import sys
    dataDownloaderObj = uniprotDownloader()
    dataDownloaderObj.options["data_path"] = "/Users/normandavey/Documents/Work/Websites/slimdb/data"
    #dataDownloaderObj.options["remake_age"] = 10000
    data = {}
    counter = 0
    
    #dataDownloaderObj.get_uniprot_accession_taxa('9606','list')
    
    pprint.pprint(dataDownloaderObj.parseUniProt("Q8N5J2",force=True))
    
    """
    for accession in open(sys.argv[1]).read().split("\n")[int(sys.argv[2]):int(sys.argv[3])]:
        counter += 1
        try:
            data.update({'MobiDB':dataDownloaderObj.parseMobiDB(accession)})
        except:
            pass
            
        try:
            data.update({'Sequence':dataDownloaderObj.parseSequence(accession)})
        except:
            pass
            
        print counter,accession,data.keys()
    

    data = {}
    data.update({'UniProt':dataDownloaderObj.parseUniProt(sys.argv[1],parse_features=True)})
    data.update({'Pfam':dataDownloaderObj.parseUniProtPfam(sys.argv[1])})
    data.update({'Attributes':dataDownloaderObj.parseAttributes(sys.argv[1])})
    data.update({'Features':dataDownloaderObj.parseFeatures(sys.argv[1])})
    data.update({'MobiDB':dataDownloaderObj.parseMobiDB(sys.argv[1])})
    pprint.pprint(data)

    #pprint.pprint(dataDownloaderObj.parseUniProt(sys.argv[1],parse_keywords=True,parse_snps=True,parse_attributes=True,parse_disorder=True,parse_isoforms=True))
    response = dataDownloaderObj.parseUniProt(sys.argv[1],parse_keywords=True,parse_snps=True,parse_attributes=True,parse_disorder=True,parse_isoforms=True)
    pprint.pprint(response)
    #pprint.pprint(dataDownloaderObj.parseUniProt(sys.argv[1],parse_keywords=True))
    """
