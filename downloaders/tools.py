
def groups(iter, keyfunc):
    """
    Makes groups of elements from an iterables by a key function on
    elements value.

    Returns a dict with key function values as key and list of elements as value.
        :param iter: An iterable.
        :param keyfunc: A function to evaluate elements from the iterables
        to keys.
    """
    result = {}
    for i in iter:
        key = keyfunc(i)
        result[key] = result.get(key,[])
        result[key].append(i)
    return result
