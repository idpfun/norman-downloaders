import os,inspect,optparse, pprint

def is_float(value):
	try:
		float(value)
		return True
	except ValueError:
		return False

def load_commandline_options(options,options_help):
	
	commandline_options = {}
	
	op = optparse.OptionParser()
	
	for option in options:
		help_text = "--Not set--"

		if option in options_help:
			help_text = options_help[option]
		
		op.add_option("--" + option,
			action="store", 
			dest=option,
			default=options[option],
			help=help_text)
	
	opts, args = op.parse_args()
	
	for option in vars(opts):
		
		try:
			if getattr(opts, option) == "True" or getattr(opts, option) == True:
				commandline_options[option] = True
			elif getattr(opts, option) == "False" or getattr(opts, option) == False:
				commandline_options[option] = False
			elif is_float(getattr(opts, option)):
				if str(getattr(opts, option)).count(".") > 0:
					commandline_options[option] = float(getattr(opts, option))
				else:
					commandline_options[option] = int(getattr(opts, option))
			else:
				
				commandline_options[option] = getattr(opts, option)
		except:
			raise
	
	return commandline_options
	