import os,re,time,string,json,pprint

from xml.dom import minidom
from xml.etree import cElementTree as elementtree

class snpDownloader():
	##------------------------------------------------------------------##

	def __init__(self):
		self.options = {}
		self.options["wait_time"] = 0.01
		self.options["data_path"] = ""
		self.options["remake_age"] = 90
		
		self.settings = {}
	
	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##
	
	def check_directory(self):
		if not os.path.exists(os.path.join(self.options["data_path"], "uniprot")):
			os.mkdir(os.path.join(self.options["data_path"], "uniprot"))
		
	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##
	
	def grabHumsavar(self):
		self.check_directory()
		
		url = "ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/variants/humsavar.txt"
		out_path = os.path.join(self.options["data_path"], "uniprot","humsavar.txt")
	
		if os.path.exists(out_path):
			if (time.time() - os.path.getctime(out_path))/60/60/24 > self.options["remake_age"] :
				os.remove(out_path)
	
		if not os.path.exists(out_path):
			try:
				cmd = "wget -O "+ out_path + " '"+url+"'"
				#print "-"*50
				#print cmd
				time.sleep(self.options["wait_time"])
				os.system(cmd)
				#print "-"*50
			
			except Exception,e:
				print e
	
	def grabSNPsByUniProt(self,accession,phenotypes=['Disease','Polymorphism','Unclassified'],residue_positions="all"):
		self.grabHumsavar()
		
		out_path = os.path.join(self.options["data_path"], "uniprot","humsavar.txt")
		
		mutations = {"mutations":{},"positions":{},"phenotype":{}}
		
		with open(out_path) as fileobject:
			for line in fileobject:
				line_bits = line.split()
				if len(line_bits) > 2:
					if line_bits[1] == accession:
						wildtype = line_bits[3][2:5]
						mutant = line_bits[3][-3:]
						position = line_bits[3][5:-3]
						variant_type = line_bits[4]
						
						
						if variant_type in phenotypes and (residue_positions == "all" or int(position) in residue_positions):
							mutations["mutations"][line_bits[2]] = {
							"position":position,
							"wildtype":wildtype,
							"mutant":position,
							"variant_type": variant_type,
							"variant": line_bits[3],
							"FTId": line_bits[2],
							"dbSNP": line_bits[5],
							"description":" ".join(line_bits[6:])
							}
						
							if line_bits[4] not in mutations["phenotype"]:
								mutations["phenotype"][line_bits[4]] = {}
						
							if position not in mutations["phenotype"][line_bits[4]]:
								mutations["phenotype"][line_bits[4]][position] = []
							
							mutations["phenotype"][line_bits[4]][position].append(line_bits[2])
						
							if position not in mutations["positions"]:
								mutations["positions"][position] = []
							
							mutations["positions"][position].append(line_bits[2])
		
		return mutations
	
if __name__ == "__main__":
	import sys
	dataDownloaderObj = snpDownloader()
	dataDownloaderObj.options["data_path"] = "/Users/normandavey/Documents/Work/Websites/slimdb/data"
	
	residue_positions = sys.argv[2]
	if residue_positions != "all": residue_positions = [int(x) for x in residue_positions.split(",")]
		
	pprint.pprint(dataDownloaderObj.grabSNPsByUniProt(sys.argv[1],residue_positions=residue_positions))
	