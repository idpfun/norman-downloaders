import os,re,time

from xml.dom import minidom
from xml.etree import cElementTree as elementtree


class pfamDownloader():
	
	def __init__(self):
		self.options = {}
		self.options["wait_time"] = 0.01
		self.options["data_path"] = ""
		self.options["remake_age"] = 1800
		
	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##
		
	def check_directory(self):
		if not os.path.exists(os.path.join(self.options["data_path"], "uniprot")):
			os.mkdir(os.path.join(self.options["data_path"], "uniprot"))
		
	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##
	
	def parseInterproPfam(self):
		self.check_directory()
		
		pfam_data = {}
		xml_path = os.path.join(self.options["data_path"] + "/pfam/",pfam_id + ".xml")
		error_pattern = re.compile("<error>.+</error>")
		
		try:
			if os.path.exists(xml_path):
				errors = error_pattern.findall(open(xml_path).read())
				if len(errors) > 0:
					return  {"status":"Error","error_type":errors[0].split(">")[1].split("<")[0]}
				
				tree = elementtree.parse(xml_path)
				root = tree.getroot()
		
				pfam_data = {"pfam_acc":"","domain_name":"","domain_description":"","clan_name":"","clan_id":""}
		
				pfam_data["pfam_id"] = pfam_id
		
				if root.find('{http://pfam.xfam.org/}entry'):	
					pfam_data["pfam_acc"]  = root.find('{http://pfam.xfam.org/}entry').attrib['id']
					for child in root.find('{http://pfam.xfam.org/}entry'):
						if child.tag.split("}")[1] == "description":
							pfam_data["domain_name"] =  child.text.strip()
						if child.tag.split("}")[1] == "comment":
							pfam_data["domain_description"] =  child.text.strip()
						if child.tag.split("}")[1] == "clan_membership":
							pfam_data['clan_id'] = child.attrib['clan_acc'].strip()
							pfam_data['clan_name'] = child.attrib['clan_id'].strip()
					
					

					
				return {"status":"Added","data":pfam_data}
			else:
				return {"status":"Error","error_type":"File not found"}
				
		except Exception,e:
			return {"status":"Error","error_type":str(e)}