import requests

URL = "https://www.uniprot.org/uploadlists/"
VALID_IDS = [ "ACC+ID", "ACC,ID", "ACC", "ID", "UPARC", "NF50", "NF90", "NF100", "GENENAME",
                "EMBL_ID", "EMBL", "P_ENTREZGENEID", "P_GI", "PIR", "REFSEQ_NT_ID",
                "P_REFSEQ_AC", "UNIGENE_ID", "PDB_ID", "DISPROT_ID", "BIOGRID_ID",
                "DIP_ID", "MINT_ID", "STRING_ID", "CHEMBL_ID", "DRUGBANK_ID",
                "GUIDETOPHARMACOLOGY_ID", "SWISSLIPIDS_ID", "ALLERGOME_ID", "ESTHER_ID",
                "MEROPS_ID", "MYCOCLAP_ID", "PEROXIBASE_ID", "REBASE_ID", "TCDB_ID",
                "BIOMUTA_ID", "DMDM_ID", "WORLD_2DPAGE_ID", "DNASU_ID", "ENSEMBL_ID",
                "ENSEMBL_PRO_ID", "ENSEMBL_TRS_ID", "ENSEMBLGENOME_ID",
                "ENSEMBLGENOME_PRO_ID", "ENSEMBLGENOME_TRS_ID", "GENEDB_ID",
                "P_ENTREZGENEID", "KEGG_ID", "PATRIC_ID", "UCSC_ID", "VECTORBASE_ID",
                "WBPARASITE_ID", "ARACHNOSERVER_ID", "ARAPORT_ID", "CCDS_ID", "CGD",
                "CONOSERVER_ID", "DICTYBASE_ID", "ECHOBASE_ID", "ECOGENE_ID",
                "EUHCVDB_ID", "EUPATHDB_ID", "FLYBASE_ID", "GENECARDS_ID",
                "GENEREVIEWS_ID", "H_INVDB_ID", "HGNC_ID", "HPA_ID", "LEGIOLIST_ID",
                "LEPROMA_ID", "MAIZEGDB_ID", "MGI_ID", "MIM_ID", "NEXTPROT_ID",
                "ORPHANET_ID", "PHARMGKB_ID", "POMBASE_ID", "PSEUDOCAP_ID", "RGD_ID",
                "SGD_ID", "TUBERCULIST_ID", "WORMBASE_ID", "WORMBASE_PRO_ID",
                "WORMBASE_TRS_ID", "XENBASE_ID", "ZFIN_ID", "EGGNOG_ID", "GENETREE_ID",
                "HOGENOM_ID", "HOVERGEN_ID", "KO_ID", "OMA_ID", "ORTHODB_ID",
                "TREEFAM_ID", "BIOCYC_ID", "REACTOME_ID", "UNIPATHWAY_ID", "CLEANEX_ID",
                "COLLECTF_ID", "CHITARS_ID", "GENEWIKI_ID", "GENOMERNAI_ID"]

def parse_mapping_response(response):
    """
    Parse the text output of uniprot mapping service
        :param response: a string with the mapping response.
        :param query: the original query list of identifiers.
    """
    if response.startswith("From"):
        lines = response.strip().split("\n")[1:]
        return {from_id: to_id for line in lines 
                for from_id, to_id in [line.split("\t")]}
    if response.startswith("yourlist:"):
        lines = response.strip().split("\n")[1:]
        fields_by_line = (line.split("\t") for line in lines)
        return {query: fields[2 if len(fields)==9 else 1] 
            for fields in fields_by_line
            for query in fields[0].split(",")}

def _add_unmapped(result, query):
    return {q:result.get(q) for q in query}

def validate_db_id(db_id):
    """
    Checks if a given id type is accepted by the uniprot mapping service.
        :param db_id: a DB id type.
    """
    return db_id in VALID_IDS

def _get_params(ids, from_id='ACC,ID', to_id='ACC'):
    params = { 
        'query' : " ".join(ids),
        'format': 'tab',
        'from': from_id,
        'to': to_id
    }
    return params

class UniprotMapping(object):
    """
    A class to consult to id mapping service of Uniprot.
        :param object: 
    """
    def __init__(self, sleep_time=0, timeout=60):
        """
        Creates a new UniprotMapping object.
            :param self: 
            :param sleep_time=0: time in seconds to wait before doing the request
            :param timeout=60: time to wait for the response from the mapping service
        """
        self.sleep_time = sleep_time
        self.timeout = timeout

    def map(self, query, from_id, to_id, log=None):
        """
        Make a request to uniprot mapping service.

        To ckeck valid identifiers see https://www.uniprot.org/help/api_idmapping
            :param self: 
            :param query: a list of identifiers
            :param from_id: a valid db identifier
            :param to_id: a valid db identifier
        """
        payload = _get_params(query, from_id, to_id)
        headers = {'user-agent': 'python'}
        response = None
        for db_id in (from_id, to_id):
            if not validate_db_id(db_id):
                raise ValueError("DB ID:'{}' not valid".format(db_id))
        try:
            response = requests.post(
                URL, 
                data=payload,
                headers=headers,
                timeout = self.timeout
                )
        except:
            print("Error while requesting [{},  ...".format(query[0]))
            pass
        if response and log:
            with open(log, 'w') as fh:
                fh.write(response.text)
        mapping = parse_mapping_response(response.text) if response else {}
        return _add_unmapped(mapping, query)

