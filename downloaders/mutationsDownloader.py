import os,re,time,string,json,pprint

from xml.dom import minidom
from xml.etree import cElementTree as elementtree

import pmidDownloader
import uniprotDownloader

class mutationsDownloader():
	##------------------------------------------------------------------##

	def __init__(self):
		self.options = {}
		self.options["wait_time"] = 0.01
		self.options["data_path"] = ""
		self.options["remake_age"] = 90
		
		self.settings = {}
	
	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##
	
	def parseMutationsByPMID(self,pmid):
		pmidDownloaderObj = pmidDownloader.pmidDownloader()
		pmidDownloaderObj.options["data_path"] = self.options["data_path"]
		return pmidDownloaderObj.parseFeaturesByPMID(pmid,['mutagenesis site'])
		
	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##


if __name__ == "__main__":
	import sys
	dataDownloaderObj = mutationsDownloader()
	dataDownloaderObj.options["data_path"] = "/Users/normandavey/Documents/Work/Websites/slimdb/data"
	pprint.pprint(dataDownloaderObj.parseMutationsByPMID(sys.argv[1]))
	
	