import requests, json, time


class slimsearchDownloader():
	#=============================================================================================

	def __init__(self):
		self.option = {}
		self.data = {}
		self.session = requests.Session()
		
		self.option["url_server"] = "http://slim.ucd.ie/rest/"


	def waiter(self,job_status):
		isRunning = True
		print "Still running",job_status,"\r"
		if job_status == "In progress":
			time.sleep(2)
		else:
			isRunning = False
		return isRunning


	def run_query(self,  params):

		url = self.option["url_server"]  + "search/"
		resp = self.session.post(url, data=json.dumps(params))

		status = resp.status_code
		if status != 200:
			print "Problem with query", params
			print "Details:", resp.json()
			raise

		jobId = resp.json()['job_id']
		return jobId


	def run_enrichment(self,  params):

		url = self.option["url_server"]  + "enrichmentREST/"
		resp = self.session.get(url = url, params=params)

		status = resp.status_code
		if status != 200:
			print "Problem with enrichment analysis", params
			print "Details:", resp.json()
			raise

		# wait for results
		isRunning = True
		while isRunning:
			resp = self.session.get(url = url, params=params)
			content = resp.json()
			isRunning = self.waiter(content['status'])

		return content['status']

	def run_conservation_protein(self, uniprot_acc):
		params = {}
		url = self.option["url_server"]  + "attributes/?uniprot_acc=" + uniprot_acc + "&type=Conservation"
		resp = self.session.get(url = url,params=params)
		status = resp.status_code
		if status != 200:
			print "Problem with conservation analysis", status
			print "Details:", resp.json()
			raise
		
		return resp.json()
		
	def run_conservation(self, params):

		url = self.option["url_server"]  + "conservation/"
		resp = self.session.get(url = url, params=params)
		
		print url
		print params

		status = resp.status_code
		if status != 200:
			print "Problem with conservation analysis", params,resp
			try:
				print "Details:", resp.json()
			except:
				pass
		else:
			# wait for results
			isRunning = True
			while isRunning:
				resp = self.session.get(url = url, params=params)
				content = resp.json()
				isRunning = self.waiter(content['status'])

			return content['status']

	def get_instances(self, params):

		url = self.option["url_server"]  + "downloadInstances/"
		resp = self.session.post(url, data = json.dumps(params))

		status = resp.status_code
		if status != 200:
			print "Problem with getting instances", params
			print "Details:", resp.json()
			raise

		isRunning = True
		while isRunning:
			resp = self.session.post(url, data=json.dumps(params))
			content = resp.json()
			if "status" in content:
				isRunning = self.waiter(resp.json()['status'])
			else:
				isRunning = False

		return content['result']


	def get_enrichment(self, params):
	
		url = self.option["url_server"]  + "downloadEnrichment/"
		resp = self.session.get(url = url, params = params)

		status = resp.status_code
		if status != 200:
			print "Problem with getting enrichment results", params
			print "Details:", resp.json()
			raise

		isRunning = True
		while isRunning:
			resp = self.session.get(url = url, params=params)
			content = resp.json()
			if "status" in content:
				isRunning = self.waiter(resp.json()['status'])
			else:
				isRunning = False

		return resp.json()['result']

	def get_conservation(self, params):

		url = self.option["url_server"]  + "downloadConservation/"
		resp = self.session.post(url, data = json.dumps(params))

		status = resp.status_code
		if status != 200:
			print "Problem with getting conservation results", params
			print "Details:", resp.json()
			raise

		isRunning = True
		while isRunning:
			resp = self.session.post(url, data=json.dumps(params))
			content = resp.json()
			if "status" in content:
				isRunning = self.waiter(resp.json()['status'])
			else:
				isRunning = False

		return resp.json()['result']
	
	def get_motif(self, params):

		url = self.option["url_server"]  + "motifEnrichment/"
		resp = self.session.get(url = url, params=params)

		status = resp.status_code
		if status != 200:
			print "Problem with enrichment analysis", params
			print "Details:", resp.json()
			raise

		# wait for results
		isRunning = True
		while isRunning:
			resp = self.session.get(url = url, params=params)
			content = resp.json()
			isRunning = self.waiter(content['status'])

		return content
	
	def get_shared_go(self, params):

		url = self.option["url_server"]  + "sharedterms"
		
		resp = self.session.post(url, data = json.dumps(params))
	
		status = resp.status_code
		if status != 200:
			print "Problem with getting shared go results", params
			print "Details:", resp.json()
			raise

		isRunning = True
		while isRunning:
			resp = self.session.post(url, data=json.dumps(params))
			content = resp.json()
			if "status" in content:
				isRunning = self.waiter(resp.json()['status'])
			else:
				isRunning = False

		return resp.json()
	
	def get_peptide_annotation(self, peptide_details):
			
		###
		###
		
		if not os.path.exists(self.option["instances_annotated_file"]):
			params = {
			"search_type": "regex",
			"regex_input": self.data["input_peptides"] ,
			"taxon_id": self.option["taxon_id"],
			"iupred": self.option["iupred_score_cutoff"],
			"flank": 0,
			"replaceLetter": True,
			"letter2replace":"A"
			}
		
			if not os.path.exists(self.option["instances_annotated_id_file"]):
				job_id = slimRestHelper.run_query(params)
				print "\n------------------------------------------------------"
				print "JobId:", str(job_id)
				open(self.option["instances_annotated_id_file"],"w").write(job_id)
			else:
				job_id = open(self.option["instances_annotated_id_file"]).read()
			
			### get motif annotations - features and attributes
			data = slimRestHelper.get_instances({"jobid": job_id, "fileFormat": "json"})
		
			with open(self.option["instances_annotated_file"], 'w') as outfile:
				json.dump(data, outfile)
		else:
			job_id = open(self.option["instances_annotated_id_file"]).read()
	
		self.data["hits"] = json.loads(open(self.option["instances_annotated_file"] , 'r').read())
			

if __name__ == "__main__":
	# set session
	
	slimRestHelper = slimsearchDownloader()
	
	tasks = ["search_re","instances","enrichment","conservation"]
	tasks = ["search_offsets"]
	tasks = ["search_re","instances"]
	
	motif = "(L..[IVLWC].E)" # regular expression used in search
	peptides = ["MEPIAIIITDTE","KFPPEITVTPPT","MSPPCISVEPPA","PGVPRITIS","DDIPTVVIHPPE","EAVPQIIISAEE","LESPRIEITSCL","GLSPRIEITPSH","FECPSIQITSIS","LECPSIRITSIS","EDLPVITIDPAS","DTTPSVVVHVCE","SSLPTFNFSSPE","TQVPTFEITSPN","MKVPTISVTSFH","LTVPSFAFEPLD","SSAPEIVVSKPE","LTSPDFALSTPD","KLIPKLSIQSPV","STNPEITITPAE","DTVPTVTVSEPE","SQIPQFTLNFVE","TNNPTITITPTP","LDVPDIIITPPT","LKIPEISIQDMT","LSIPDVKITQHF","LAVPAVSVDRKG","LFIPNITVNNSG","TSNPTVQLKIPQ","WTNPQFKISLPE","TSLPVITVSLPP","RPPPLVEITPRD"]
	pssm = {"S":[0.47,1.62,-0.17,-3.53,1.14,-5.02,1.54,-4.94,2.33,1.51,-0.39,0.24],"T":[1.09,1.89,-0.45,-3.71,2.09,-3.5,2.83,-3.3,4.05,-0.31,0.53,0.1],"G":[-1.7,-0.66,-3.87,-4.98,-2.17,-6.41,-2.61,-6.4,-2.68,-2.57,-3.45,-0.06],"A":[-1.92,0.16,-1.15,-3.49,0.56,-3.91,0.22,-3.72,-1.48,-0.68,-0.72,-0.87],"P":[-0.5,-0.04,2.43,7.68,-2.16,-5.7,-2.47,-5.53,-2.67,4.07,5.03,0.02],"C":[-3.06,-2.59,5.34,-5.47,2.47,-4.07,-2.57,-3.87,-3.09,-3.02,5.1,-3.57],"I":[-1.41,-2.3,4.35,1.31,-2.11,7.72,1.95,7.89,-3.39,-0.31,0.52,-2.87],"L":[3,-0.3,0.85,-5.04,-1.25,0.67,-2.11,2.11,-3.62,-0.97,-1.09,-2.12],"V":[-1.7,-1.98,3.54,-4.34,-0.43,4.36,1.01,4.63,-2.73,-0.85,-0.58,-0.76],"M":[3.34,-1.63,-1.5,-4.81,-1.7,-1.67,-1.66,-1.41,-2.7,-2.08,-0.04,-2.48],"F":[0.63,1.11,-2.96,-6.06,-3.24,6.58,-3.19,3.66,-4.04,0.38,-0.2,0.05],"Y":[-2.37,-2.04,-3.39,-5.61,-2.64,-2.48,-2.8,-3.3,-2.91,-2.64,-3,-2.39],"W":[3.3,-3.19,-4.62,-6.46,-3.6,-4.27,-3.89,-4.85,-4.31,-3.88,-4.25,-3.8],"H":[-2.83,-1.79,-3.71,-4.99,-1.69,-5.56,-2.24,-5.8,1.93,-2.37,0.21,1.44],"K":[0.15,0.09,-3.24,-3.76,0.39,-5.51,0.23,-5.37,-0.18,-0.71,-0.88,-1.16],"R":[-0.58,-1.41,-3.71,-4.92,1.01,-5.68,-0.65,-5.57,-1.87,-0.77,-0.93,-1.86],"D":[0.63,0.73,-3.32,-4.27,0.98,-6.15,-1.69,-6.08,0.69,0.96,-2.96,1.77],"E":[-0.36,1.44,-3.44,-3.89,0.94,-5.9,0.97,-5.79,0.38,-0.81,-1.39,3.09],"N":[-2.12,-0.17,1.61,-4.82,0.18,-6.02,-0.12,-5.99,1.35,-0.18,-2.78,-0.29],"Q":[-1.68,0.75,-3.22,-4.05,1.39,-5.54,0.91,-5.37,1.15,-0.42,-2.21,0.59]}
	
	pssm_score_cutoff = 20 
	numResults = 500
	mask_cutoff = 0.5
	mask = ""
	taxon_id = 9606 # proteome (human)
	iupred_cutoff = 0 # accessiblity cut-off, return instances only with score above 0.4 (score is calculated per each instance as mean of IUPred scores across motif residues). Range: 0-1. The lower score, the more globular regions.
	flank = 3 # flank length for searched motif

	if "search_re" in tasks:
		params = {
			"search_type": "regex",
			"regex_input": [motif],
			"taxon_id": taxon_id,
			"iupred": iupred_cutoff,
			"flank": flank,
			"numResults":100000
		}

		run_query = True

	if "search_pssm" in tasks:
		params = { 
		"taxon_id": taxon_id, 
		"search_type": "pwmJSON", 
		"pwm_json": pssm,
		"iupred": iupred_cutoff, 
		"flank": flank, 
		"pssm_score": pssm_score_cutoff, 
		"numResults": numResults, 
		"mask_cutoff": mask_cutoff, 
		"motif": mask
		}
		
		run_query = True
	
	if "search_peptides" in tasks:
		params = {
			"search_type": "regex",
			"regex_input": peptides,
			"taxon_id": taxon_id,
			"iupred": iupred_cutoff,
			"flank": flank,
			"replaceLetter": False
		}

		run_query = True
	
	if "search_offsets" in tasks:
		params = {
			"search_type": "regex",
			"regex_input": [motif],
			"taxon_id": taxon_id,
			"iupred": iupred_cutoff,
			"flank": flank
		}
		
		run_query = True
	
	if run_query:
		# job_id = "rdFRPtBhbk034vsX5pp0YnhJLqWh7zW7"
		job_id = slimRestHelper.run_query(params)
		print "\n------------------------------------------------------"
		print "JobId:", str(job_id)
	
	
	if "instances" in tasks:
		### get motif annotations - features and attributes
		data = slimRestHelper.get_instances({"jobid": job_id, "fileFormat": "json"})
		if len(data['result']) > 0:
			print "\n------------------------------------------------------"
			print "First record", data['result'][0]


	############################################################################
	### run enrichment analysis of functional annotations (GOterms, KW, interactors)
	### Enrichment params:
	if "enrichment" in tasks:
		params_enrichment = {
			"cluster": "uniref50",
			"id": "Biological process", # you have to pass something - this is only useful for website
			"conservation": "True",
			"jobid": job_id
		}
	
		status = slimRestHelper.run_enrichment(s, params_enrichment)
		if status != "Finished":
			print "Something is wrong with enrichment analysis"
		else:
			## get results
			params_enrichment['id'] = "all"
			params_enrichment['fileFormat'] = "json"
			params_enrichment['con'] = params_enrichment['conservation']
			enrichment = slimRestHelper.get_enrichment(s, params_enrichment)
			if len(enrichment['Biological process']['data']) > 0:
				print "\n------------------------------------------------------"
				print "Example record", enrichment['Biological process']['data'][0]

	############################################################################
	### run conservation analysis
	### Conservation params:
	if "conservation" in tasks:
		params_con = {
			"jobid": job_id,
			"searchDb": "QFO"
		}

		status = slimRestHelper.run_conservation(s, params_con)
		if status != "Finished":
			print "Something is wrong with conservation analysis"
		else:
			## get results
			params_con['clad'] = params_con['searchDb']
			params_con['fileFormat'] = "json"
			con = slimRestHelper.get_conservation(s, params_con)
			if len(con['conservation']) > 0:
				print "\n------------------------------------------------------"
				print "Conservation - Example record: ", con['conservation'][0]
