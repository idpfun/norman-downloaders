import os
from os import mkdir
from os.path import join
from os.path import exists
import re
import time
import requests

from xml.dom import minidom
from xml.etree import cElementTree as elementtree


class pfamDownloader():
    def __init__(self):
        self.options = {}
        self.options["wait_time"] = 0.01
        self.options["data_path"] = ""
        self.options["remake_age"] = 1800
        
    def check_directory(self):
        if not exists(join(self.options["data_path"], "pfam")):
            mkdir(join(self.options["data_path"], "pfam"))
        
    def grabPfam(self, pfam_id):
        self.check_directory()
        
        url = 'https://pfam.xfam.org/family/' + pfam_id + '?output=xml'
        out_path = join(self.options["data_path"], "pfam", pfam_id + ".xml")
    
        if exists(out_path):
            if (time.time() - os.path.getctime(out_path))/60/60/24 > self.options["remake_age"] :
                os.remove(out_path)
        
        if not exists(out_path):
            time.sleep(self.options["wait_time"])
            response = requests.get(url)
            with open(out_path, 'w') as fh:
                fh.write(response.text)
            return response.text

    def parsePfam(self,pfam_id):
        self.grabPfam(pfam_id)
        xml_path = os.path.join(self.options["data_path"] + "/pfam/",pfam_id + ".xml")
        error_pattern = re.compile("<error>.+</error>")
        try:
            if os.path.exists(xml_path):
                errors = error_pattern.findall(open(xml_path).read())
                if len(errors) > 0:
                    return  {"status":"Error","error_type":errors[0].split(">")[1].split("<")[0]}
                
                tree = elementtree.parse(xml_path)
                root = tree.getroot()

                pfam_data = {"pfam_acc":"","domain_name":"","domain_description":"","clan_name":"","clan_id":""}
                pfam_data["pfam_id"] = pfam_id
        
                if root.find('{https://pfam.xfam.org/}entry'):	
                    pfam_data["pfam_acc"]  = root.find('{https://pfam.xfam.org/}entry').attrib['id']
                    for child in root.find('{https://pfam.xfam.org/}entry'):
                        if child.tag.split("}")[1] == "description":
                            pfam_data["domain_name"] =  child.text.strip()
                        if child.tag.split("}")[1] == "comment":
                            pfam_data["domain_description"] =  child.text.strip()
                        if child.tag.split("}")[1] == "clan_membership":
                            pfam_data['clan_id'] = child.attrib['clan_acc'].strip()
                            pfam_data['clan_name'] = child.attrib['clan_id'].strip()
                    
                return {"status":"Added","data":pfam_data}
            else:
                return {"status":"Error","error_type":"File not found"}
        except Exception,e:
            return {"status":"Error","error_type":str(e)}
