class Singleton(type):
    """
    Simple class to aesy implement singletons.
    Please, do not over use singletons.
        :param type: 
    """
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]
