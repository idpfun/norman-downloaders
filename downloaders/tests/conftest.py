import inspect
from os.path import dirname
from os.path import join
from pytest import fixture

@fixture(scope="session")
def test_data_folder():
    return join(dirname(inspect.stack()[0][1]), "data")
