
from downloaders.pdbDownloader import pdbDownloader
from os.path import exists
from os.path import join
from os.path import getsize
from os import remove
from pytest import fixture

@fixture(scope="module")
def mol_file_2ast(test_data_folder):
    mol_file = join(test_data_folder,'pdb', "2AST.molecule.xml")
    if exists(mol_file):
        remove(mol_file)
    yield mol_file
    if exists(mol_file):
        remove(mol_file)

@fixture(scope="module")
def pdb_file_2ast(test_data_folder):
    mol_file = join(test_data_folder,'pdb', "2AST.xml")
    if exists(mol_file):
        remove(mol_file)
    yield mol_file
    if exists(mol_file):
        remove(mol_file)


@fixture(scope="module")
def struct_file_2ast(test_data_folder):
    mol_file = join(test_data_folder,'pdb', "2AST.pdb")
    if exists(mol_file):
        remove(mol_file)
    yield mol_file
    if exists(mol_file):
        remove(mol_file)

@fixture(scope="module")
def pfam_file_2ast(test_data_folder):
    mol_file = join(test_data_folder,'pdb', "2AST.pfam.xml")
    if exists(mol_file):
        remove(mol_file)
    yield mol_file
    if exists(mol_file):
        remove(mol_file)

class TestpdbDownloader(object):
    def test_grabMoleculePDB(self, test_data_folder, mol_file_2ast):
        dl = pdbDownloader(test_data_folder)
        dl.options['pdbID'] = "2AST"
        assert not exists(mol_file_2ast)
        dl.grabMoleculePDB()
        assert exists(mol_file_2ast)
        assert getsize(mol_file_2ast) > 0

    def test_grabPDB(self, test_data_folder, pdb_file_2ast):
        dl = pdbDownloader(test_data_folder)
        dl.options['pdbID'] = "2AST"
        assert not exists(pdb_file_2ast)
        dl.grabPDB()
        assert exists(pdb_file_2ast)
        assert getsize(pdb_file_2ast) > 0

    def test_grabPDBStructure(self, test_data_folder, struct_file_2ast):
        dl = pdbDownloader(test_data_folder)
        dl.options['pdbID'] = "2AST"
        assert not exists(struct_file_2ast)
        dl.grabPDBStructure()
        assert exists(struct_file_2ast)
        assert getsize(struct_file_2ast) > 0  

    def test_grabPfamPDB(self, test_data_folder, pfam_file_2ast):
        dl = pdbDownloader(test_data_folder)
        dl.options['pdbID'] = "2AST"
        assert not exists(pfam_file_2ast)
        dl.grabPfamPDB()
        assert exists(pfam_file_2ast)
        assert getsize(pfam_file_2ast) > 0       
