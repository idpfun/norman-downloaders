from os import remove
from os.path import exists
from os.path import join
from pytest import fail
from pytest import fixture
import re
from re import match
from downloaders.pfamDownloader import pfamDownloader

class TestPfamDownloader(object):

    @fixture(scope='module')
    def file_pf00131(self):
        file = join("downloaders", "tests", "pfam", "PF00131.xml")
        if exists(file):
            remove(file)
        yield file
        if exists(file):
            remove(file)
        
    def test_grabPfam(self, file_pf00131):
        pfam_id = "PF00131"
        dl = pfamDownloader()
        dl.options["data_path"] = join("downloaders", "tests")
        dl.grabPfam(pfam_id)
        content = open(file_pf00131, 'r').read()
        assert "Metallothionein" in content
        assert "https://pfam.xfam.org/" in content
        pfam_pattern = re.compile(".*<pfam[^<>]*>.*</pfam>.*", re.DOTALL)
        assert match(pfam_pattern,content)

    def test_parsePfam(self, file_pf00131):
        pfam_id = "PF00131"
        dl = pfamDownloader()
        dl.options["data_path"] = join("downloaders", "tests")
        pfam_data = dl.parsePfam(pfam_id)
        assert len(pfam_data) == 2
        assert "pfam_acc" in pfam_data["data"]
        assert "domain_name" in pfam_data["data"]
        assert "domain_description" in pfam_data["data"]
        assert "clan_name" in pfam_data["data"]
        assert "clan_id" in pfam_data["data"]
