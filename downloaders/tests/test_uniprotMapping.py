from downloaders.uniprotMapping import validate_db_id
from downloaders.uniprotMapping import parse_mapping_response
from downloaders.uniprotMapping import UniprotMapping


def test_validate_db_id():
    assert validate_db_id("ACC,ID")
    assert validate_db_id("ACC+ID")
    assert not validate_db_id("acc+id")
    assert validate_db_id("ID")
    assert validate_db_id("ACC")

def test_parse_mapping_response():
    sample_response = "From	To\nTACC1_HUMAN	O75410\nSC5A3_HUMAN	P53794\n" + \
                      "MP3B2_HUMAN	A6NCE7"
    parsed = parse_mapping_response(sample_response)
    assert len(parsed) == 3
    assert parsed["TACC1_HUMAN"] == "O75410"
    assert parsed["SC5A3_HUMAN"] == "P53794"
    assert parsed["MP3B2_HUMAN"] == "A6NCE7"

    sample_response = "yourlist:M201811078A530B6CA0138AFAA6D2B97CE8C2A92401BC461	"+\
                      "isomap:M201811078A530B6CA0138AFAA6D2B97CE8C2A92401BC461	Entry	"+\
                      "Entry name	Status	Protein names	Gene names	Organism	"+\
                      "Length\nCOR2A_HUMAN		Q92828	COR2A_HUMAN	reviewed	"+\
                      "Coronin-2A (IR10) (WD repeat-containing protein 2)	CORO2A "+\
                      "IR10 WDR2	Homo sapiens (Human)	525\nFRIL_HUMAN		P02792"+\
                      "	FRIL_HUMAN	reviewed	Ferritin light chain (Ferritin L "+\
                      "subunit)	FTL	Homo sapiens (Human)	175\nENPP6_HUMAN		"+\
                      "Q6UWR7	ENPP6_HUMAN	reviewed	Ectonucleotide pyrophosphatase/"+\
                      "phosphodiesterase family member 6 (E-NPP 6) (NPP-6) (EC 3.1.4.-) "+\
                      "(EC 3.1.4.38) (Choline-specific glycerophosphodiester "+\
                      "phosphodiesterase) (Glycerophosphocholine cholinephosphodiesterase) "+\
                      "(GPC-Cpde)	ENPP6 UNQ1889/PRO4334	Homo sapiens (Human)	440"
    parsed = parse_mapping_response(sample_response)
    assert len(parsed) == 3
    assert parsed["COR2A_HUMAN"] == "Q92828"
    assert parsed["FRIL_HUMAN"] == "P02792"
    assert parsed["ENPP6_HUMAN"] == "Q6UWR7"

    sample_response = "yourlist:M20181108A7434721E10EE6586998A056CCD0537EC4F0BDX	Entry	"+\
                      "Entry name	Status	Protein names	Gene names	Organism	Length\n"+\
                      "UBE2Z_HUMAN	Q9H832	UBE2Z_HUMAN	reviewed	Ubiquitin-conjugating "+\
                      "enzyme E2 Z (EC 2.3.2.23) (E2 ubiquitin-conjugating enzyme Z) (Uba6-"+\
                      "specific E2 conjugating enzyme 1) (Use1) (Ubiquitin carrier protein Z)"+\
                      " (Ubiquitin-protein ligase Z)	UBE2Z HOYS7	Homo sapiens (Human)	354\n"+\
                      "EAN57_HUMAN,TEX33_HUMAN	O43247	TEX33_HUMAN	reviewed	Testis-expressed"+\
                      " protein 33	TEX33 C22orf33 EAN57	Homo sapiens (Human)	280"
    parsed = parse_mapping_response(sample_response)
    assert len(parsed) == 3
    assert parsed["UBE2Z_HUMAN"] == "Q9H832"
    assert parsed["EAN57_HUMAN"] == "O43247"
    assert parsed["TEX33_HUMAN"] == "O43247"


class TestUniprotMapping(object):
    def test_map(self):
        um = UniprotMapping()
        result = um.map(
            query=["UBE2Z_HUMAN", "TEX33_HUMAN"],
            from_id="ID",
            to_id="ACC",
            log='log.txt',
            )
        assert len(result) == 2
        assert result["UBE2Z_HUMAN"] == "Q9H832"
        assert result["TEX33_HUMAN"] == "O43247"
        assert not result.get("NON_EXISTENT")
