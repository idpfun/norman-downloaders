
from downloaders.elmDownloader import elmDownloader


class TestElmDownloader():
    def test_init(self):
        downloader = elmDownloader()
        assert not downloader.options["debug"]
        assert downloader.options["aligned_type"] == "peptides_aligned"
