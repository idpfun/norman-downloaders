from os.path import exists
from os.path import join
from os import remove
from pprint import pprint
import pytest
from pytest import fail
from pytest import fixture
from downloaders.interactionDownloader import interactionDownloader

@fixture(scope="module")
def setup_teardown(test_data_folder):
    files_to_remove = [
        join(test_data_folder, "uniprot", "Q09472.xml"),
        join(test_data_folder, "interaction", "hippie_uniprot_id_mapping.json"),
        join(test_data_folder, "grab_hippie", "interaction", "hippie_uniprot_id_mapping.json"),
        join(test_data_folder, "grab_hippie", "interaction", "HIPPIE-current.json"),
    ]
    for f in files_to_remove:
        if exists(f):
            remove(f)    
    yield
    for f in files_to_remove:
        if exists(f):
            remove(f)

@fixture(scope="module")
def downloader(test_data_folder):
    return interactionDownloader(test_data_folder)

class TestInteractionDownloader(object):

    def test_load_interactions(self, downloader, setup_teardown):
        assert len(downloader.interactions_by_pmid["16189514"]) == 2

    def test_get_interaction(self, downloader, setup_teardown):
        downloader.options["cutoff"] = 0.75
        data = downloader.getInteractions(["10531037"],"pmid")
        assert not data.get("data",{}).get("10531037")
        assert data["status"] == "Success"

        data = downloader.getInteractions(["16189514"],"pmid")
        assert "16189514" in data["data"]
        assert "PX0004" in data["data"]["16189514"]["PX0003"]
        assert "PX0003" in data["data"]["16189514"]["PX0004"]
        assert "PX0006" in data["data"]["16189514"]["PX0005"]
        assert "PX0005" in data["data"]["16189514"]["PX0006"]
        assert data["status"] == "Success"

        data = downloader.getInteractions(["PX0005"],"accession")
        assert "1122334455" in data["data"]["PX0005"]["PX0004"]["pmids"]
        assert data["data"]["PX0005"]["PX0004"]["pmids"]["1122334455"] == {
            'confidence': '0.76',
            'method':  ["MI:0493(in vivo)", "MI:0018(two hybrid)"],
            'source': ["MI:0468(hprd)", "biogrid", "MI:0469(intact)", "MI:0471(mint)", "i2d", "rual05"]
        }
        assert data["status"] == "Success"

        data = downloader.getInteractions(["PX0007"],"accession")
        assert "1100000001" in data["data"]["PX0007"]["PX0008"]["pmids"]
        assert "1100000002" in data["data"]["PX0007"]["PX0008"]["pmids"]        
        assert data["status"] == "Success"

        data = downloader.getInteractions(["PX0005","PX0007", "PX0008"], 'pairwise')
        assert len(data) == 2
        assert "1100000001" in data["data"]["PX0007"]["PX0008"]["pmids"]
        assert "1100000002" in data["data"]["PX0007"]["PX0008"]["pmids"]

    def test_hippie_data_mappings(self, downloader, setup_teardown):
        mapping = downloader.hippie_data_mappings(['EGFR_HUMAN'], True)
        assert len(mapping) == 1
        assert mapping['EGFR_HUMAN'] == "P00533"

    def test_getDomainInteractions(self, downloader, setup_teardown):
        response = downloader.getDomainInteractions("Q99814", "PF08214")
        assert "data" in response
        assert "Q09472" in response["data"]
        assert "1100000002" in response["data"]["Q09472"]
        assert sorted(response["data"]["Q09472"]["1100000002"].get("domains", "")) == \
            ['PF00439', 'PF00569', 'PF02135', 'PF02172', 'PF06001', 'PF08214', 'PF09030']

    def test_grabHIPPIE(self, downloader, setup_teardown):
        downloader.set_data_path(join(downloader.data_path, "grab_hippie"))
        downloader.grabHIPPIE()
        downloader.load_interactions()
        assert "P00352" in downloader.interactions_by_acc
        assert len(downloader.interactions_by_acc.get("P00352", [])) == 6
        assert all(["P00352" in x["a_accession"] and "P00352" in x["b_accession"] 
                for x in downloader.interactions_by_acc.get("P00352", [])])
        assert {x["pmid"] for x in downloader.interactions_by_acc.get("P00352", [])} == {"12081471", "16189514", "25416956"}
