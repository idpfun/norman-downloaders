import os,re,time,string, json

from xml.dom import minidom
from xml.etree import cElementTree as elementtree

import downloaders.uniprotDownloader as uniprotDownloader

class pmidDownloader():
	
	def __init__(self):
		self.options = {}
		self.options["wait_time"] = 0.01
		self.options["data_path"] = ""
		self.options["remake_age"] = 1800
		
	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##
	
	def check_directory(self):
		if not os.path.exists(os.path.join(self.options["data_path"], "pmid")):
			os.mkdir(os.path.join(self.options["data_path"], "pmid"))
		
	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##
	
	def grabPMID(self,pmid):
		self.check_directory()
		
		url = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&id=' + pmid + '&report=xml'
		out_path = os.path.join(self.options["data_path"] + "/pmid/",pmid + ".xml")
		#print out_path, os.path.exists( os.path.join(self.options["data_path"] + "/pmid/"))
		
		if os.path.exists(out_path):
			if (time.time() - os.path.getctime(out_path))/60/60/24 > self.options["remake_age"] :
				os.remove(out_path)
	
		if not os.path.exists(out_path):
			try:
				cmd = "wget -O "+ out_path + " '"+url+"'"
			
				time.sleep(self.options["wait_time"])
				os.system(cmd)
			
			except Exception,e:
				print e

	##------------------------------------------------------------------##
	
	def parsePMID(self,pmid):
		article_data = {"pmid":pmid}
	
		header_mapping = {
		'ArticleTitle':"title",
		'Authors':"author_list",
		'ISOAbbreviation':"journal",
		'Volume':"article_volume",
		'Issue':"article_issue",
		'MedlinePgn':"article_page",
		'ArticleId':"article_details",
		'AbstractText':"abstract_text",
		'PDB':"pdb",
		'Year':"article_year",
		'Month':"article_month",
		}

		try:
			self.grabPMID(pmid)
			
			xml_path = os.path.join(self.options["data_path"] + "/pmid/",pmid + ".xml")
			#print open(xml_path).read()
			
			error_pattern = re.compile("<error>.+</error>")
			
			if os.path.exists(xml_path):
				errors = error_pattern.findall(open(xml_path).read())
				
				if len(errors) > 0:
					return  {"status":"Error","error_type":errors[0].split(">")[1].split("<")[0]}
				
				tree = elementtree.parse(xml_path)
				root = tree.getroot()

				for tag_name in ['ArticleTitle',"AbstractText",'ISOAbbreviation','Volume','Issue','MedlinePgn']:
					article_data[header_mapping[tag_name]] = ""
					for elem in root.iter(tag_name):
						try:
							
							article_data[header_mapping[tag_name]] = filter(lambda x: x in string.printable, elem.text)
						except:
							pass
		
				authors = []
				for elem in tree.iter('Author'):
					authors_dict = {"ForeName":"", "LastName":"","Initials":""}
					try:
						authors_dict["ForeName"] = elem.find("ForeName").text
					except:
						pass
			
					try:
						authors_dict["LastName"] = elem.find("LastName").text
					except:
						pass
				
					try:
						authors_dict["Initials"] = elem.find("Initials").text 
					except:
						pass
		
					authors.append(authors_dict)
				
				authors_short = ""
				authors_long = ""
				
				try:
					if len(authors) == 1:
						authors_short = authors[0]['LastName'] + " "  +authors[0]['Initials']
					else:
						authors_short = authors[0]['LastName']  + " et al"

					if len(authors) == 1:
						authors_long = authors[0]['LastName'] + " "  +authors[0]['Initials']
					else:
						authors_long = ""
						authors_tmp = []
		
						for author in authors:
							authors_tmp += [author['LastName'] + " " + author['Initials']]
			
						if len(authors_tmp) > 1:
							authors_long = ", ".join(authors_tmp[:-1]) + " and " + authors_tmp[-1]
						elif len(authors_tmp) == 1:
							authors_long = authors_short
				except:
					pass

				article_data["short_author"] = authors_short
				article_data["long_author"] = authors_long
				article_data["author_list"] = authors
				article_data["article_month"] = ""
				article_data["article_year"] = ""
				
				mesh_terms = []
				for elem in tree.iter('MeshHeading'):
					try:
						descriptorName = elem.find("DescriptorName").text
						mesh_terms.append(descriptorName)
					except:
						pass

				article_data["mesh_terms"] = mesh_terms
					
				for elem in tree.iter('PubDate'):
					for dates in elem.getchildren():
						try:
							article_data[header_mapping[dates.tag]] = dates.text
						except:
							pass
							
				
				article_data["pdbs"] = []
				for elem in tree.iter('DataBank'):
					if elem.find("DataBankName").text == "PDB":
						for databank in elem.find("AccessionNumberList").getchildren():
							article_data["pdbs"].append(databank.text)
				
				try:
					article_data["article_details"] = article_data['article_volume'] + '(' + article_data['article_issue']  + '):' + article_data['article_page']
				except:
					article_data["article_details"] = ""
		
				try:
					article_data["proteins"] = self.parseUniProtByPMID(pmid,True)['data']
				except:
					article_data["proteins"] = {}
					
				#print article_data
				
				return {"status":"Success","data":article_data}
			else:
				return {"status":"Error","error_type":"File not found"}
		except Exception,e:
			return {"status":"Error","error_type":str(e)}
	
	##------------------------------------------------------------------##
	
	def grabUniProtByPMID(self,pmid):
		
		self.check_directory()
		
		url = "http://www.uniprot.org/uniprot/?sort=&desc=&query=(citation:" + pmid + ")&fil=&force=no&format=tab&columns=id,entry%20name,reviewed,protein%20names,genes,organism,length,sequence"
		out_path = os.path.join(self.options["data_path"] + "/pmid/",pmid + ".uniprot.tdt")
		
		if os.path.exists(out_path):
			if (time.time() - os.path.getctime(out_path))/60/60/24 > self.options["remake_age"]:
				os.remove(out_path)
	
		if not os.path.exists(out_path):
			try:
				cmd = "wget -O "+ out_path + " '"+url+"'"
				#print "-"*50
				#print cmd
				time.sleep(self.options["wait_time"])
				os.system(cmd)
				#print "-"*50
			
			except Exception,e:
				print e

	##------------------------------------------------------------------##
	
	def grabAnnotationByPMID(self,pmid):
		
		self.check_directory()
		
		url = "https://www.ebi.ac.uk/europepmc/annotations_api/annotationsByArticleIds?articleIds=MED%3A" + pmid + "&type=Gene_Proteins&format=JSON"

		out_path = os.path.join(self.options["data_path"] + "/pmid/",pmid + ".annotation.json")
		
		if os.path.exists(out_path):
			if (time.time() - os.path.getctime(out_path))/60/60/24 > self.options["remake_age"]:
				os.remove(out_path)
	
		if not os.path.exists(out_path):
			try:
				cmd = "wget -O "+ out_path + " '"+url+"'"
				#print "-"*50
				print url
				print cmd
				time.sleep(self.options["wait_time"])
				os.system(cmd)
				#print "-"*50
			
			except Exception,e:
				print e
	
	##------------------------------------------------------------------##

	def parseAnnotationByPMID(self,pmid,detailed=False):
		self.grabAnnotationByPMID(pmid)
		
		json_path = os.path.join(self.options["data_path"] + "/pmid/",pmid + ".annotation.json")
		
		annotation_json_data = {}
		annotation_data = {}
		
		if detailed:
			uniprotDownloaderObj = uniprotDownloader.uniprotDownloader()
			uniprotDownloaderObj.options["data_path"] = self.options["data_path"]

		if os.path.exists(json_path):
			with open(json_path) as outfile:
				annotation_data = json.load(outfile)
			
			
				if 'annotations' in annotation_data[0]:
					for annotation in annotation_data[0]['annotations' ]:
						annotation_json_data[annotation['tags'][0]['uri'].split("/")[-1]] = annotation['tags'][0]['name']
						
					if detailed:
						for accession in annotation_json_data:
							try:
								try:
									protein_data = uniprotDownloaderObj.parseBasic(accession)['data']
								except:
									protein_data = {}
								
								tag = annotation_json_data[accession]
								annotation_json_data[accession] = protein_data
								annotation_json_data[accession]['tag'] = tag
							except Exception,e:
								print "Error",accession,pmid,e
								#raise
		
			return {"status":"Success","data":annotation_json_data}
		else:
			return {"status":"Error","error_type":"File not found"}
		
		
	##------------------------------------------------------------------##
	
	def parseUniProtByPMID(self,pmid,detailed=False):
		self.grabUniProtByPMID(pmid)
	
		tdt_path = os.path.join(self.options["data_path"] + "/pmid/",pmid + ".uniprot.tdt")
		
		if os.path.exists(tdt_path):
			if detailed:
				uniprotDownloaderObj = uniprotDownloader.uniprotDownloader()
				uniprotDownloaderObj.options["data_path"] = self.options["data_path"]


			tdt_data = open(tdt_path).read().strip().split("\n")
	
			headers = tdt_data[0].split("\t")
	
			proteins = []
			for protein in tdt_data[1:]:
				accession = protein.split("\t")[0]
				proteins.append(accession)
		
		
			if detailed:
				annotation_json_data = {}
				for accession in proteins:
				
					protein_data = uniprotDownloaderObj.parseBasic(accession)
				
					if protein_data['status'] == "Success":
						annotation_json_data[accession] = protein_data['data']
			
				return {"status":"Success","data":annotation_json_data}
			else:
				return proteins
		else:
			return proteins
		
	##------------------------------------------------------------------##
	
	def parseFeaturesByPMID(self,pmid,features="all"):
		try:
			uniprotDownloaderObj = uniprotDownloader.uniprotDownloader()
			uniprotDownloaderObj.options["data_path"] = self.options["data_path"]

			accessions = self.parseUniProtByPMID(pmid)
			feature_data = {}
		
			for accession in accessions:
				protein_data = uniprotDownloaderObj.parseFeatures(accession)
			
				if 'features' in protein_data['data']:
					for feature_type in protein_data['data']['features']:
						if feature_type in features or features == "all":
							for feature_instance in protein_data['data']['features'][feature_type]:
								if pmid in feature_instance['pmid']:
									if accession not in feature_data:
										feature_data[accession] = {}
									if feature_type not in feature_data[accession]:
										feature_data[accession][feature_type] = []
									
									feature_data[accession][feature_type].append(feature_instance)
		
			return {"status":"Success","data":feature_data}
		except Exception,e:
			return {"status":"Error","error_type":str(e)}
	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##
