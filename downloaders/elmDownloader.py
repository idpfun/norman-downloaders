import sys
import os
import urllib
import time
import traceback
import pprint
import re
import json
import copy
import inspect
from hashlib import md5

from downloaders.uniprotDownloader import uniprotDownloader
import downloaders.config_reader as config_reader

id_mapping = {
    "PF14604":"PF00018",
    "PF07653":"PF00018",
    "PF12763":"PF00036",
    "SM00054":"PF00036",
    "PF13499":"PF00036",
    "SM00028":"PF00515",
    "IPR017986":"PF00400"
}


class elmDownloader():
	def __init__(self):
		
		self.options = {}
		
		self.options["debug"] = False
		self.options['aligned_type'] = 'peptides_aligned'
		
		config_options = config_reader.load_configeration_options(sections=["general","structure_reader"])
		
		for config_option in config_options:
			self.options[config_option] = config_options[config_option]
			
	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def check_directory(self):
		if not os.path.exists(os.path.join(self.options["data_path"], "elm")):
			os.mkdir(os.path.join(self.options["data_path"], "elm"))
		
	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##
	
	def grabELMDomains(self):
		self.check_directory()
		
		out_path = os.path.join(self.options["data_path"],'elm','elm_domains.tsv')
		
		others = """TRG_Cilium_RVxP_2	Unknown	Unknown	Unknown
LIG_UBA3_1	PF00899	ThiF	ThiF family/Repeat in ubiquitin-activating (UBA) protein
LIG_SUMO_SIM_par_1	PF11976	Rad60-SLD	Ubiquitin-2 like Rad60 SUMO-like/Ubiquitin family
LIG_SUMO_SIM_anti_2	PF11976	Rad60-SLD	Ubiquitin-2 like Rad60 SUMO-like/Ubiquitin family
LIG_OCRL_FandH_1	PF00620	RhoGAP	RhoGAP domain
LIG_GBD_WASP_1	PF00786	PDB	P21-Rho-binding domain
LIG_CAP-Gly_2	PF01302	CAP_GLY	CAP-Gly domain
LIG_APCC_Cbox_2	PF00515	TPR_1	Tetratricopeptide repeat
LIG_APCC_Cbox_1	PF00515	TPR_1	Tetratricopeptide repeat
CLV_PCSK_KEX2_1	PF00082	Peptidase_S8	Subtilase family
CLV_MEL_PAP_1	PF00089	Trypsin	Trypsin"""

		if os.path.exists(out_path):
			if (time.time() - os.path.getctime(out_path))/60/60/24 >  self.options["remake_age"]:
				os.remove(out_path)
	
		if not os.path.exists(out_path):
			url = 'http://elm.eu.org/infos/browse_elm_interactiondomains.tsv'
			print("Grabbing",url)
			
			try:
				opener = urllib.FancyURLopener()
				f = opener.open(url).read()
				file_content = f.split('\n')
				version = file_content[0].split(':')[-1].strip()
				file_content = [x for x in file_content if not x.startswith('#')]

				open(out_path,'w').write( '\n'.join(file_content) + "\n" + others )

			except Exception,e:
				print e
		
	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def grabELMInstances(self):
		self.check_directory()
		
		out_path = os.path.join(self.options["data_path"],'elm','elm_instances.tsv')
		
		if os.path.exists(out_path):
			if (time.time() - os.path.getctime(out_path))/60/60/24 >  self.options["remake_age"]:
				os.remove(out_path)
		
		if not os.path.exists(out_path):
			url = 'http://elm.eu.org/instances.tsv?q=*'
			print "Grabbing",url
			
			dataDownloaderObj = uniprotDownloader()
			dataDownloaderObj.options["data_path"] = self.options["data_path"]
		
			try:
				opener = urllib.FancyURLopener()
				f = opener.open(url).read()
				file_content = f.split('\n')
				version = file_content[0].split(':')[-1].strip()
				file_content = [x for x in file_content if not x.startswith('#')]
				
				file_content_str = file_content[0].strip() + '\tsequence\tprotein_name\tfamily\tspecies_common\ttaxonomy\n'
				
				for line in file_content[1:]:
					if len(line.split("\t")) > 1:
						accession = line.split("\t")[4].strip('"')
						start =  line.split("\t")[6].strip('"')
						end =  line.split("\t")[7].strip('"')
					
						protein_data = dataDownloaderObj.parseUniProt(accession)
						protein_sequence = dataDownloaderObj.parseUniProtFasta(accession)
						
						if "sequence" not in protein_sequence:
							pass
						else:
							
							file_content_str += line.strip()
					
							#if protein_data['status'] == "Error":
							#	file_content_str += "\t" + "\t".join(["","","","",""])
							#else:
							extras = []
							extras.append(protein_sequence["sequence"])
						
							if 'data' in protein_data:
								extras.append(protein_data['data']['protein_name'])
								extras.append(protein_data['data']['family'])
								extras.append(protein_data['data']['species_common'])
								extras.append(protein_data['data']['taxonomy'])
							else:
								extras += ["","","",""]
					
							file_content_str += "\t" + "\t".join(extras)
					
							file_content_str += "\n"
					
				open(out_path,'w').write(file_content_str)

			except Exception,e:
				print e
	
	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def grabELMInteractions(self):
		self.check_directory()
		
		out_path = os.path.join(self.options["data_path"],'elm','elm_interactions.tsv')
			
		if os.path.exists(out_path):
			if (time.time() - os.path.getctime(out_path))/60/60/24 >  self.options["remake_age"]:
				os.remove(out_path)
	
		if not os.path.exists(out_path):
			url = 'http://elm.eu.org/interactions/as_tsv'
			print "Grabbing",url
			
			try:
				opener = urllib.FancyURLopener()
				f = opener.open(url).read()
				file_content = f.split('\n')
				version = file_content[0].split(':')[-1].strip()
				file_content = [x for x in file_content if not x.startswith('#')]

				open(out_path,'w').write('\n'.join(file_content))

			except Exception,e:
				print e
	
	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def grabELMClasses(self):
		self.check_directory()
		
		self.grabELMInstances()
		tsv_path = os.path.join(self.options["data_path"],"elm","elm_instances.tsv")
		elm_instances = self.parseTDT(tsv_path,"ELMIdentifier")
		
		out_path = os.path.join(self.options["data_path"],'elm','elm_classes.tsv')
			
		if os.path.exists(out_path):
			if (time.time() - os.path.getctime(out_path))/60/60/24 >  self.options["remake_age"]:
				os.remove(out_path)
	
		if not os.path.exists(out_path):
			url = 'http://elm.eu.org/elms/elms_index.tsv'
			print "Grabbing",url
			
			try:
				opener = urllib.FancyURLopener()
				f = opener.open(url).read()
				file_content = f.strip().split('\n')
				version = file_content[0].split(':')[-1].strip()
				file_content = [x.strip() for x in file_content if not x.startswith('#')]
				
				file_content_with_additions = [file_content[0] + '\t"Flexible_Length"']
				
				for line in file_content[1:]:
					line_bits = line.split("\t")
					ELMIdentifier = line_bits[1].strip('"')
			
					lengths = []
					
					if ELMIdentifier in elm_instances:
						for instance in elm_instances[ELMIdentifier]:
							lengths.append(int(instance['End']) - int(instance['Start']))
				
						#print ELMIdentifier,"\t",len(set(lengths)) == 1,lengths
			
					line = "\t".join(line_bits) + "\t" + '"' + str(len(set(lengths)) > 1) + '"'
					file_content_with_additions.append(line)
			
				open(out_path,'w').write('\n'.join(file_content_with_additions))
				
			except Exception,e:
				print e
				raise
		
		
	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##
	
	def getAlignedELMClasses(self,elm_class):
		self.check_directory()
		
		tsv_path = os.path.join(self.options["data_path"],"elm","elm_instances.tsv")
		
		elm_protein_instances = self.parseELM()
		elm_instances = {}
		for protein in elm_protein_instances['data'].keys():
			for elm_instance_id in elm_protein_instances['data'][protein]:
				
				ELMIdentifier = elm_protein_instances['data'][protein][elm_instance_id]['ELMIdentifier']
				peptide = elm_protein_instances['data'][protein][elm_instance_id]['sequence'][int(elm_protein_instances['data'][protein][elm_instance_id]['Start'])-1:int(elm_protein_instances['data'][protein][elm_instance_id]['End'])]
				
				if ELMIdentifier not in elm_instances:
					elm_instances[ELMIdentifier] = {}
					
				elm_instances[ELMIdentifier][elm_instance_id] = peptide
		
		elm_class_peptides = list(set(elm_instances[elm_class].values()))
		elm_class_peptides.sort()
				
		elm_class_alignment_path = os.path.join(self.options["data_path"],"elm",elm_class +  "." + md5(",".join(elm_class_peptides)).hexdigest() + ".clustalw.alignment.json")
		
		
		if not os.path.exists(elm_class_alignment_path):
			print "Missing",elm_class_alignment_path
			self.alignELMClasses()
	
		if os.path.exists(elm_class_alignment_path):
			with open(elm_class_alignment_path) as data_file:
				return json.load(data_file)
		else:
			return {}
	
		
	
	def alignELMClasses(self):
		tsv_path = os.path.join(self.options["data_path"],"elm","elm_instances.tsv")
		
		elm_protein_instances = self.parseELM()
		elm_instances = {}
		for protein in elm_protein_instances['data'].keys():
			for elm_instance_id in elm_protein_instances['data'][protein]:
				
				ELMIdentifier = elm_protein_instances['data'][protein][elm_instance_id]['ELMIdentifier']
				peptide = elm_protein_instances['data'][protein][elm_instance_id]['sequence'][int(elm_protein_instances['data'][protein][elm_instance_id]['Start'])-1:int(elm_protein_instances['data'][protein][elm_instance_id]['End'])]
				
				if ELMIdentifier not in elm_instances:
					elm_instances[ELMIdentifier] = {}
					
				elm_instances[ELMIdentifier][elm_instance_id] = peptide
				#elm_instances[''] = elm_protein_instances['data'][protein]
		
		for elm_class in elm_instances:
			
			forward_dict = {}
			reverse_dict = {}
			
			for elm_instance in elm_instances[elm_class]:
			
				accession = elm_instance
				peptide = elm_instances[elm_class][elm_instance]
				
				forward_dict[accession] = peptide
				
				if peptide not in reverse_dict:
					reverse_dict[peptide] = []
					
				reverse_dict[peptide].append(accession)
			
			peptides = list(set(forward_dict.values()))
			peptides.sort()
			
			elm_class_alignment_path = os.path.join(self.options["data_path"],"elm",elm_class +  "." + md5.new(",".join(peptides)).hexdigest() + ".clustalw.alignment.json")

			
			if not os.path.exists(elm_class_alignment_path):
				print "Getting",elm_class_alignment_path
				url = "http://slim.ucd.ie/didi/tools/peptides/align/clustalw/json/peptides/" + ",".join(peptides)
				
				#print "Grabbing",url
				alignment_dict = {"peptides_pssm_aligned":{},"peptides_aligned":{}}
				
				try:
					opener = urllib.FancyURLopener()
					f = json.loads(opener.open(url).read())
					
					if "peptides_pssm_aligned" in f:
						for peptide in f["peptides_pssm_aligned"]:
							for id in reverse_dict[peptide.replace("-","")]:
								alignment_dict['peptides_pssm_aligned'][id] = peptide
								
					if "peptides" in f:
						for peptide in f["peptides"]:
							for id in reverse_dict[peptide.replace("-","")]:
								alignment_dict['peptides_aligned'][id] = peptide
					
					#print 
					#print "#",elm_class
					#for id in alignment_dict['peptides_aligned']:
					#	print id,"\t",alignment_dict['peptides_aligned'][id],"\t",alignment_dict['peptides_pssm_aligned'][id],"\t", elm_instances[elm_class][id]
						
					with open(elm_class_alignment_path, 'w') as outfile:
						json.dump(alignment_dict, outfile)
					
					print alignment_dict
				except Exception,e:
					print e
	
	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##
	
	def parseTDT(self,tsv_path,tdt_key):
		tdt_data = {}
		headers = []
		
		for line in open(tsv_path).read().strip().split("\n"):
			if len(line) > 0:
				
				lineBits = line.strip().replace('"','').split("\t")
				if line[0] == "#" or len(lineBits) <= 3:
					pass
				else:
					if headers == []:
						headers = lineBits
					else:
						try:
							if lineBits[headers.index(tdt_key)] not in tdt_data:
								tdt_data[lineBits[headers.index(tdt_key)]]= []
						
							tmp_dict = {}
							for i in range(0,len(headers)):
								if len(lineBits) > i:
									tmp_dict[headers[i]] = lineBits[i]
								else:
									tmp_dict[headers[i]] = "?"
									
							tdt_data[lineBits[headers.index(tdt_key)]].append(tmp_dict)
							
						except Exception,e:
							print "skipping line",lineBits
							print e
		
		return tdt_data

	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##

	def getInteractions(self,instances):
		tsv_path = os.path.join(self.options["data_path"],"elm","elm_interactions.tsv")
		interactions = self.parseTDT(tsv_path,"Elm")
		
		interaction_dict = {}
		interaction_dict_tmp = {}
		for elm_class in interactions:
			for interaction in interactions[elm_class]:
				interaction_list = [elm_class]
				for val in ['interactorElm','StartElm','StopElm']:
					interaction_list.append(interaction[val])
				
				interaction_dict_tmp["_".join(interaction_list)] = interaction
		
		for instance in instances:
			try:
				tmp_id = "_".join([ instances[instance][0]['ELMIdentifier'], instances[instance][0]['Primary_Acc'], instances[instance][0]['Start'], instances[instance][0]['End']])
				if tmp_id in interaction_dict_tmp:
					if instance not in interaction_dict:
						interaction_dict[instance] = []
					
					interaction_dict[instance].append(interaction_dict_tmp[tmp_id])
			except:
				print "skipping instance",instance
				
		return interaction_dict
	
	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##
	def parseELMClasses(self):
		self.grabELMClasses()
		
		tsv_path = os.path.join(self.options["data_path"],"elm","elm_classes.tsv")
		classes = self.parseTDT(tsv_path,"ELMIdentifier")
		
		return classes
	
	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##
	
	def parseELMDomains(self):
		self.grabELMDomains()
		
		tsv_path = os.path.join(self.options["data_path"],"elm","elm_domains.tsv")
		domains = self.parseTDT(tsv_path,"Interaction Domain Id")
			
		return domains
			
	def parseELM(self):
		self.grabELMInstances()
		self.grabELMClasses()
		self.grabELMDomains()
		self.grabELMInteractions()
		
		try:
			tsv_path = os.path.join(self.options["data_path"],"elm","elm_instances.tsv")
			instances = self.parseTDT(tsv_path,"Accession")
			
			tsv_path = os.path.join(self.options["data_path"],"elm","elm_classes.tsv")
			classes = self.parseTDT(tsv_path,"ELMIdentifier")
			
			tsv_path = os.path.join(self.options["data_path"],"elm","elm_domains.tsv")
			domains = self.parseTDT(tsv_path,"ELM identifier")
			
			interactions = self.getInteractions(instances)
			
			elm_data = {}
			for instance in instances:
				try:
					if instances[instance][0]["Primary_Acc"] not in elm_data:
						elm_data[instances[instance][0]["Primary_Acc"]] = {}
				
					if instances[instance][0]["ELMIdentifier"] not in ["LIG_KEPE_1","LIG_KEPE_2","LIG_KEPE_3"]:
						try:
							for domain in domains[instances[instance][0]["ELMIdentifier"]]:
								if domain['Interaction Domain Id'] in id_mapping:
									instances[instance][0]["binding_domain_id"] = id_mapping[domain['Interaction Domain Id']]
									instances[instance][0]["binding_domain_name"] = domain['Interaction Domain Name']
									instances[instance][0]["binding_domain_description"] = domain['Interaction Domain Description']
								else:
									instances[instance][0]["binding_domain_id"] = domain['Interaction Domain Id']
									instances[instance][0]["binding_domain_name"] = domain['Interaction Domain Name']
									instances[instance][0]["binding_domain_description"] = domain['Interaction Domain Description']
						except:
							instances[instance][0]["binding_domain_id"] =  "Unknown"
							instances[instance][0]["binding_domain_name"] =  "Unknown"
							instances[instance][0]["binding_domain_description"] = "Unknown"
					
						pattern = re.compile(classes[instances[instance][0]["ELMIdentifier"]][0]['Regex'])
						peptide = instances[instance][0]['sequence'][int(instances[instance][0]['Start'])-1:int(instances[instance][0]['End'])]
						
						
						if len(pattern.findall(peptide)) == 0:
							pass#print "Skipping instance. Incorrect Mapping UniProt",instance,instances[instance][0]["Primary_Acc"],peptide,classes[instances[instance][0]["ELMIdentifier"]][0]['Regex']
						else:
							elm_data[instances[instance][0]["Primary_Acc"]][instance] = instances[instance][0]
					
							if instance in interactions:
								 elm_data[instances[instance][0]["Primary_Acc"]][instance]["interactions"] = interactions[instance]
							else:
								 elm_data[instances[instance][0]["Primary_Acc"]][instance]["interactions"] = []
				except:
					print "Skipping instance",instance
					
			return {"status":"Found","data":elm_data}
			
		except Exception,e:
			error_type = sys.exc_info()[0]
			error_value = sys.exc_info()[1]
			error_traceback = traceback.extract_tb(sys.exc_info()[2])
			
			sys.stderr.write("\n")
			sys.stderr.write('Error in routine\n')
			sys.stderr.write('Error Type       : ' + str(error_type) + '\n')
			sys.stderr.write('Error Value      : ' + str(error_value) + '\n')
			sys.stderr.write('File             : ' + str(error_traceback[-1][0]) + '\n')
			sys.stderr.write('Method           : ' + str(error_traceback[-1][2]) + '\n')
			sys.stderr.write('Line             : ' + str(error_traceback[-1][1]) + '\n')
			sys.stderr.write('Error            : ' + str(error_traceback[-1][3]) + '\n')
			
			if self.options["debug"]:
				raise
				
			return {"status":"Error","error_type":str(e)}
			
	##------------------------------------------------------------------##
	##
	##------------------------------------------------------------------##
	
	def get_elm_classes(self,options):
		try:
			elm_classes = self.parseELMClasses()
			json_data = {}
		
			if options['identifier'] == "all":
				if options['identifier_type'] == "details":
					json_data = elm_classes
				if options['identifier_type'] == "identifiers":
					json_data = elm_classes.keys()
				if options['identifier_type'] == "names":
					json_data = {}
					for elm_class in elm_classes:
						json_data[elm_class] = elm_classes[elm_class][0]["FunctionalSiteName"]
			else:
				if options['identifier'] in elm_classes:
					json_data = elm_classes[options['identifier']]
		
			if len(json_data) > 0:
				return {"status": "Found", "data": json_data}
			else:
				return {"status": "Error", "error_type": "Entry not found"}
		
		except Exception,e:
			return {"status": "Error", "error_type": e}
			
	def get_elm_instances(self,options):
		try:
			if 'aligned_type' not in options:
				options['aligned_type'] = "peptides_aligned"
			
			if 'max_gap_proportion' not in options:
				options['max_gap_proportion'] = 1
				
			if 'flanks' in options:
				options['flanks'] = int(options['flanks'])
			else:
				options['flanks'] = 0
				
			elm_data = self.parseELM()
			elm_data = elm_data['data']
			
			protein_list = []
		
			if 'get_protein_list' in options:
				if options["get_protein_list"]:
					for protein in elm_data:
						for instance in elm_data[protein]:
							protein_list.append(elm_data[protein][instance]["Primary_Acc"] )
							
					return {"status": "Found", "data": list(set(protein_list))}
			
			domain_list = []
			if 'get_domain_list' in options:
				if options["get_domain_list"]:
					for protein in elm_data:
						for instance in elm_data[protein]:
							domain_list.append(elm_data[protein][instance]["binding_domain_id"] )

					return {"status": "Found", "data": list(set(domain_list))}
					
					
			#-------------------
			#-------------------
			
			peptides = {}
			json_data = {}
			aligned_data = {}
	
			if options['identifier_type'] == "elm" and options['identifier'] != "all":
				aligned_data = self.getAlignedELMClasses(options['identifier'])
			
			use_instances = []
		
			
			for protein in elm_data:
				for instance in elm_data[protein]:
			
					use = False
					if options['identifier_type'] == "uniprot":
						if elm_data[protein][instance]["Primary_Acc"] == options['identifier'] or options['identifier'] == "all":
							use_instances.append(instance)
							protein_list.append(elm_data[protein][instance]["Primary_Acc"] )

					if options['identifier_type'] == "elm":
						if elm_data[protein][instance]['ELMIdentifier'] == options['identifier'] or options['identifier'] == "all":
							use_instances.append(instance)
					
					if options['identifier_type'] == "organism":
						if elm_data[protein][instance]['Organism'] == options['identifier'] or options['identifier'] == "all":
							use_instances.append(instance)
							
					if options['identifier_type'] == "pfam":
						if elm_data[protein][instance]["binding_domain_id"] == options['identifier'] or options['identifier'] == "all":
							use_instances.append(instance)
			
			#-------------------
			#-------------------
			
			flanks_lengths = {}
			for aligned_type in ['peptides_aligned','peptides_padded_aligned',u'peptides_pssm_aligned','peptides_padded_pssm_aligned']:
				flanks_lengths[aligned_type] = {"right":{},"left":{}}
			
			#-------------------
			#-------------------
			
			for protein in elm_data:
				for instance in elm_data[protein]: 
					if instance in use_instances:
						start =  int(elm_data[protein][instance]['Start'])
						end =  int(elm_data[protein][instance]['End'])

						l_flank = elm_data[protein][instance]["sequence"][max(0,start- options['flanks'] - 1):start-1].rjust(options['flanks'],"-")
						peptide = elm_data[protein][instance]["sequence"][start-1:end]
						r_flank = elm_data[protein][instance]["sequence"][end:min(end+options['flanks'],len(elm_data[protein][instance]["sequence"]))].ljust(options['flanks'],"-")

						elm_data[protein][instance]['l_flank'] = l_flank
						elm_data[protein][instance]['peptide'] = peptide
						elm_data[protein][instance]['extended_peptide'] = l_flank + peptide + r_flank
						elm_data[protein][instance]['r_flank'] = r_flank
				
						peptides[instance] = elm_data[protein][instance]['extended_peptide']
						
						
						if u'peptides_aligned' in aligned_data:
							if instance in aligned_data[u'peptides_aligned']:
								for aligned_type in ['peptides_aligned','peptides_pssm_aligned']:
									try:
										aligned_peptide = aligned_data[aligned_type][instance]
										
										left_pad = len(aligned_peptide) - len(aligned_peptide.lstrip("-"))
										right_pad = len(aligned_peptide) - len(aligned_peptide.rstrip("-"))
										left_flank = elm_data[protein][instance]["l_flank"]
										right_flank = elm_data[protein][instance]["r_flank"]
										
										aligned_padded_peptide = "-"*left_pad + left_flank + aligned_peptide.strip("-") + right_flank + "-"*right_pad 
										aligned_padded_peptide = "-"*left_pad + left_flank + aligned_peptide.strip("-") + right_flank + "-"*right_pad 
									
										#if aligned_type == 'peptides_aligned' or 'peptides_aligned' not in elm_data[protein][instance]:
										if aligned_type == 'peptides_aligned':
											elm_data[protein][instance]['peptides_aligned'] = aligned_peptide
											elm_data[protein][instance]['peptides_padded_aligned'] = aligned_padded_peptide
											
										if aligned_type == 'peptides_pssm_aligned':
											elm_data[protein][instance]['peptides_pssm_aligned'] = aligned_peptide
											elm_data[protein][instance]['peptides_padded_pssm_aligned'] = aligned_padded_peptide
										
									except:
										raise
								
						instance_data = copy.deepcopy(elm_data[protein][instance])
						
						del instance_data["sequence"]
						json_data[instance] = instance_data
						del instance_data
			
			#-------------------
			#-------------------
			
			for instance in json_data: 
				for aligned_type in ['peptides_aligned','peptides_padded_aligned',u'peptides_pssm_aligned','peptides_padded_pssm_aligned']:
					if aligned_type in json_data[instance]:
						aligned_peptide = json_data[instance][aligned_type]
						left_pad = len(aligned_peptide) - len(aligned_peptide.lstrip("-"))
						right_pad = len(aligned_peptide) - len(aligned_peptide.rstrip("-"))
			
						flanks_lengths[aligned_type]["left"][instance] = left_pad
						flanks_lengths[aligned_type]["right"][instance] = right_pad
		
			#-------------------
			#-------------------
			
			columns = {}
			
			
			if options['identifier_type'] == "elm":
				for aligned_type in ['peptides_aligned','peptides_padded_aligned',u'peptides_pssm_aligned','peptides_padded_pssm_aligned']:
					
					for instance in json_data:
						left_flank = min(flanks_lengths[aligned_type]['left'].values())
						right_flank = min(flanks_lengths[aligned_type]['right'].values())
						
						peptide = json_data[instance][aligned_type]
					
						json_data[instance][aligned_type] = json_data[instance][aligned_type][left_flank:len(peptide)-right_flank]
					
						if aligned_type == options['aligned_type']:
							for i in range(0,len(json_data[instance][aligned_type])):
								if i not in columns:
									columns[i] = []
							
								columns[i].append(json_data[instance][aligned_type][i])
							 
							peptides[instance] = json_data[instance][aligned_type]
			
				use_columns = []
				for i in range(0,len(columns)):
					if (1 - float(columns[i].count("-"))/len(columns[i])) > options['max_gap_proportion']:
						use_columns.append(i)
			
				if len(use_columns) == 0:
					use_columns = range(0,len(columns))
				
				for instance in peptides:
					for i in range(0,len(peptides[instance])):
						peptides[instance] = json_data[instance][aligned_type][min(use_columns):max(use_columns)]
			
			#-------------------
			#-------------------
			
			if 'format' in options:
				if options['format'] == "peptides":
					return {"status": "Found", "data": peptides}
					
				if options['format'] == "fasta":
					fasta = ""
					for elm_id in json_data:
						fasta += ">sp|" + json_data[elm_id]['Primary_Acc'] + "|" + json_data[elm_id]['ProteinName'] +  " " +  elm_id + " " + json_data[elm_id]['Start']  + "-" +  json_data[elm_id]['End']  + " OS=" +  json_data[elm_id]['Organism']  + "\n"
						fasta += peptides[elm_id] + "\n"
						
					return {"status": "Found", "data": fasta}
			
			#-------------------
			#-------------------
			
			if len(json_data) > 0:
				return {"status": "Found", "data": json_data}
			else:
				return {"status": "Error", "error_type": "Entry not found"}
				
		except Exception,e:
			raise
			return {"status": "Error", "error_type": e}
			
if __name__ == "__main__":
	
	dataDownloaderObj = elmDownloader()
	
	dataDownloaderObj.grabELMClasses()
	
	response = dataDownloaderObj.parseELM()
	
	elm_data = response["data"]
	
	pfam_acc = "PF00018"
	ELMIdentifier = "LIG_SH3_2"
	flanks = 5
	
	for protein in elm_data:
		for instance in elm_data[protein]:
			#if elm_data[protein][instance]['ELMIdentifier'] == ELMIdentifier:
			#if elm_data[protein][instance]["binding_domain_id"] == pfam_acc:
			#if elm_data[protein][instance]["Primary_Acc"] == "P06400":
				for header in ['Primary_Acc','ProteinName','Start','End','binding_domain_id','ELMIdentifier',"PDB","protein_name","species_common"]:
					print elm_data[protein][instance][header],'\t',
				
				start =  int(elm_data[protein][instance]['Start'])
				end =  int(elm_data[protein][instance]['End'])
				
				print elm_data[protein][instance]["sequence"][max(0,start- flanks - 1):start-1].rjust(5,"-"),
				print elm_data[protein][instance]["sequence"][start-1:end],
				print elm_data[protein][instance]["sequence"][end:min(end+flanks,len(elm_data[protein][instance]["sequence"]))].ljust(5,"-"),
				print 
					
					
