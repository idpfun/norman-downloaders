"""
Creates the required file "HIPPIE-current.json".
Requires three text files:
 - HIPPIE-current.mitab.txt: Can be extracted from http://cbdm-01.zdv.uni-mainz.de/~mschaefer/hippie/download.php
 - missing_mappings_in_hippie.tab: I don't know
 - hippie_uniprot_id_mapping.tab: Mapping ids using uniprot mapping rest service.
     This file can be created with this tool setting the option '--fetch' to True.
"""

import sys
import optparse
from pprint import pprint

from downloaders.interactionDownloader import interactionDownloader

def main():
    parser = optparse.OptionParser()
    parser.add_option('-d', '--datapath', action="store", dest="data_path", default=".")
    parser.add_option('-f', '--fetch', action="count", dest="fetch", default=False)
    parser.add_option('-x', '--force_download', action="count", dest="force_download", default=False)
    options, _ = parser.parse_args()
    dl = interactionDownloader(data_path=options.data_path)
    dl.grabHIPPIE(
        fetch_mapping=options.fetch,
        force_download=options.force_download
        ) # pylint: disable=E1101

if __name__ == "__main__":
    main()
