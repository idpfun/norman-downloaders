import sys
def version():
    with open("VERSION", "r") as fh:
        return fh.readline()

def version_tuple(version):
    v = [int(x) for x in version.split(".")] + [0, 0]
    return v[0:3]

def save_version(version):
    with open("VERSION", 'w') as fh:
        fh.write(".".join([str(i) for i in version]))

version = version_tuple(version())

if "update_major" in sys.argv:
    version = (version[0]+1, 0, 0)
    save_version(version)
    sys.argv.remove("update_major")

if "update_minor" in sys.argv:
    version = (version[0], version[1]+1, 0)
    save_version(version)
    sys.argv.remove("update_minor")

if "update_patch" in sys.argv:
    version = (version[0], version[1], version[2]+1)
    save_version(version)
    sys.argv.remove("update_patch")

output = """from setuptools import setup
setup(name='downloaders',
    version='{}',
    description='Norman Davey`s downloaders',
    url='',
    author='Norman Davey',
    license='I dont know yet',
    scripts=['bin/grab_hippie.py'],
    packages=['downloaders'],
    zip_safe=False)""".format(".".join([str(i) for i in version]))

with open("setup.py", 'w') as fh:
    fh.write(output)
